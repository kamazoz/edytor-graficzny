﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing
{
    enum CMYK { Cyan, Magenta, Yellow, Black };
    class CmykClass
    {
        static public float[] RgbToCmyk(byte[] rgb)
        {
            float[] cmyk = new float[rgb.Length];

            for (int i = 0; i < rgb.Length; i += 4)
            {
                if (rgb[i + 2] == 0 && rgb[i + 1] == 0 && rgb[i + 0] == 0)
                {
                    cmyk[i + (int)CMYK.Cyan] = cmyk[i + (int)CMYK.Magenta] = cmyk[i + (int)CMYK.Yellow] = 0;
                    cmyk[i + (int)CMYK.Black] = 1;
                }
                else
                {
                    cmyk[i + (int)CMYK.Cyan] = (float)(1 - rgb[i + 2] / 255.0);
                    cmyk[i + (int)CMYK.Magenta] = (float)(1 - rgb[i + 1] / 255.0);
                    cmyk[i + (int)CMYK.Yellow] = (float)(1 - rgb[i + 0] / 255.0);
                    float minCmyk = Math.Min(Math.Min(cmyk[i + (int)CMYK.Cyan], cmyk[i + (int)CMYK.Magenta]), cmyk[i + (int)CMYK.Yellow]);

                    cmyk[i + (int)CMYK.Cyan] = (cmyk[i + (int)CMYK.Cyan] - minCmyk) / (1 - minCmyk);
                    cmyk[i + (int)CMYK.Magenta] = (cmyk[i + (int)CMYK.Magenta] - minCmyk) / (1 - minCmyk);
                    cmyk[i + (int)CMYK.Yellow] = (cmyk[i + (int)CMYK.Yellow] - minCmyk) / (1 - minCmyk);
                    cmyk[i + (int)CMYK.Black] = minCmyk;

                }
            }
            return cmyk;
        }
        static public byte[] CmykToRgb(float[] cmyk)
        {
            byte[] rgb = new byte[cmyk.Length];

            for (int i = 0; i < cmyk.Length; i += 4)
            {
                float oneMinusB = 1 - cmyk[i + (int)CMYK.Black];
                float cyan = cmyk[i + (int)CMYK.Cyan];
                float magenta = cmyk[i + (int)CMYK.Magenta];
                float yellow = cmyk[i + (int)CMYK.Yellow];
                rgb[i + 3] = 255;
                rgb[i + 2] = (byte)(255 * (oneMinusB - cyan * oneMinusB));
                rgb[i + 1] = (byte)(255 * (oneMinusB - magenta * oneMinusB));
                rgb[i + 0] = (byte)(255 * (oneMinusB - yellow * oneMinusB));
            }
            return rgb;
        }
    }
}
