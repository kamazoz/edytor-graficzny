﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {

        //Morfologia
        private void dylatacja(object sender, RoutedEventArgs e)
        {
            kopia.Source = Morfologia.Dylatacja(obraz);
        }
        private void erozja(object sender, RoutedEventArgs e)
        {
            kopia.Source = Morfologia.Erozja(obraz);
        }
        private void otwarcie(object sender, RoutedEventArgs e)
        {
            kopia.Source = Morfologia.Otwarcie(obraz);
        }
        private void domkniecie(object sender, RoutedEventArgs e)
        {
            kopia.Source = Morfologia.Domkniecie(obraz);
        }
        private void pogrubienie(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            kopia.Source = Morfologia.Pogrubienie(o);
        }
        private void pocienienie(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            kopia.Source = Morfologia.Pocienienie(o);
        }
    }
    class Morfologia
    {
        static int norma = 9;
        static int[,] maskaThickening = { { 255, 255, -1 }, { 255, 0, -1 }, { 255, -1, 0 } };
        static int[,] maskaThinning = { { 0, 0, 0 }, { -1, 255, -1 }, { 255, 255, 255 } };
        static int[,] maska = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
        
        static public BitmapSource Domkniecie(Obraz obraz)
        {
            Obraz nowy = new Obraz(Dylatacja(obraz));
            return Erozja(nowy);
        }
        static public BitmapSource Otwarcie(Obraz obraz)
        {
            Obraz nowy = new Obraz(Erozja(obraz));
            return Dylatacja(nowy);
        }

        static private byte[,] hitormiss(Obraz obraz, int[,] mask)
        {
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            float gray;
            int a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    stary2D[i, j] = nowyObraz[a];
                    nowy2D[i, j] = nowyObraz[a++];
                }

            }
            for (int y = 0; y < obraz.pixelHeight; ++y)
            {
                for (int x = 0; x < obraz.stride; x += 4)
                {
                    gray = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 12; j += 4)
                        {
                            int ny = y - 1 + i;
                            int nx = x - 4 + j;
                            if (mask[i, j / 4] == -1)
                                gray++;
                            else
                            {
                                if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                                {
                                    if (mask[i, j / 4] == 0)
                                        gray++;
                                }
                                else
                                {
                                    if (mask[i, j / 4] == stary2D[ny, nx])
                                        gray++;
                                }
                            }
                        }
                    }
                    if (gray == 9)
                    {
                        nowy2D[y, x] = nowy2D[y, x + 1] = nowy2D[y, x + 2] = 255;
                    }
                    else
                    {
                        nowy2D[y, x] = nowy2D[y, x + 1] = nowy2D[y, x + 2] = 0;
                    }


                }
            }
            return nowy2D;
        }
        static private int[,] obroc90(int[,] mask)
        {
            int b = 0;
            int[,] newMask = new int[3, 3];
            for (int i = 0; i < 3; ++i)
            {
                b = 0;
                for (int j = 2; j >= 0; --j)
                {
                    newMask[b++, i] = mask[i, j];
                }
            }
            return newMask;
        }
        static public BitmapSource Pogrubienie(Obraz obraz)
        {
            int[,] mask = maskaThickening;
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D90 = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D180 = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D270 = new byte[obraz.pixelHeight, obraz.stride];
            nowy2D = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D90 = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D180 = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D270 = hitormiss(obraz, mask);

            int a = 0;
            float b = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    b = 0;
                    if (nowy2D[i, j] > 0)
                        b += 255;
                    if (nowy2D90[i, j] > 0)
                        b += 255;
                    if (nowy2D180[i, j] > 0)
                        b += 255;
                    if (nowy2D270[i, j] > 0)
                        b += 255;
                    if (b >= 255)
                        b = 255;
                    nowyObraz[a] = (byte)Math.Abs(nowyObraz[a] - b);
                    a++;
                }

            }
            return obraz.KlonujObraz(nowyObraz);
        }
        static public BitmapSource Pocienienie(Obraz obraz)
        {
            int[,] mask = maskaThinning;
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D90 = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D180 = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D270 = new byte[obraz.pixelHeight, obraz.stride];
            nowy2D = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D90 = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D180 = hitormiss(obraz, mask);
            mask = obroc90(mask);
            nowy2D270 = hitormiss(obraz, mask);

            int a = 0;
            float b = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    b = 0;
                    if (nowy2D[i, j] > 0)
                        b += 255;
                    if (nowy2D90[i, j] > 0)
                        b += 255;
                    if (nowy2D180[i, j] > 0)
                        b += 255;
                    if (nowy2D270[i, j] > 0)
                        b += 255;
                    if (b >= 255)
                        b = 255;
                    nowyObraz[a] = (byte)Math.Abs(nowyObraz[a] - b);
                    a++;
                }

            }
            return obraz.KlonujObraz(nowyObraz);
        }
        static public BitmapSource Erozja(Obraz obraz)
        {
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            int a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    stary2D[i, j] = nowyObraz[a];
                    nowy2D[i, j] = nowyObraz[a++];
                }

            }
            float gray = 0;
            for (int y = 0; y < obraz.pixelHeight; ++y)
            {
                for (int x = 0; x < obraz.stride; x += 4)
                {
                    gray = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 12; j += 4)
                        {
                            int ny = y - 1 + i;
                            int nx = x - 4 + j;
                            if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                            {
                                gray += maska[i, j / 4] * 255;

                            }
                            else
                            {
                                gray += maska[i, j / 4] * stary2D[ny, nx];
                            }
                        }
                    }
                    if (gray > 0 && gray < norma * 255)
                    {
                        nowy2D[y, x] = nowy2D[y, x + 1] = nowy2D[y, x + 2] = 0;
                    }
                }
            }
            a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    nowyObraz[a++] = nowy2D[i, j];
                }

            }
            return obraz.KlonujObraz(nowyObraz);

        }
        static public BitmapSource Dylatacja(Obraz obraz)
        {
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            int a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    stary2D[i, j] = nowyObraz[a];
                    nowy2D[i, j] = nowyObraz[a++];
                }

            }
            float gray = 0;
            for (int y = 0; y < obraz.pixelHeight; ++y)
            {
                for (int x = 0; x < obraz.stride; x += 4)
                {
                    gray = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 12; j += 4)
                        {
                            int ny = y - 1 + i;
                            int nx = x - 4 + j;
                            if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                            {
                                gray += maska[i, j / 4] * 255;

                            }
                            else
                            {
                                gray += maska[i, j / 4] * stary2D[ny, nx];
                            }
                        }
                    }
                    if (gray > 0)
                    {
                        nowy2D[y, x] = nowy2D[y, x + 1] = nowy2D[y, x + 2] = 255;
                    }
                }
            }
            a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    nowyObraz[a++] = nowy2D[i, j];
                }

            }
            return obraz.KlonujObraz(nowyObraz);

        }
    }

}
