﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using SysColor = System.Drawing.Color;
using SysRectangle = System.Drawing.Rectangle;

namespace ImageProcessing
{
    public class PPM

    {
        private PPMHeader header;
        public PPMHeader Header
        {
            get { return header; }
            set { header = value; }
        }

        private byte[] imageData;
        public byte[] ImageData
        {
            get { return imageData; }
            set { imageData = value; }
        }

        private PixelFormat pixelFormat;
        public PixelFormat PixelFormat
        {
            get { return pixelFormat; }
        }

        private int bytesPerPixel;
        public int BytesPerPixel
        {
            get { return bytesPerPixel; }
        }

        private int stride;
        public int Stride
        {
            get { return stride; }
            set { stride = value; }
        }

        private Bitmap bitmap;
        public Bitmap BitMap
        {
            get { return bitmap; }
        }
        public PPM(string filename)
        {
            if (File.Exists(filename))
            {
                FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                this.FromStream(stream);
                stream.Close();
            }
            else
            {
                throw new FileNotFoundException("The file " + filename + " does not exist", filename);
            }
        }

        public PPM(Stream stream)
        {
            this.FromStream(stream);
        }

        private void FromStream(Stream stream)
        {
            int index;
            this.header = new PPMHeader();
            int headerItemCount = 0;
            BinaryReader binReader = new BinaryReader(stream);
            try
            {
                while (headerItemCount < 4)
                {
                    char nextChar = (char)binReader.PeekChar();
                    if (nextChar == '#')
                    {
                        while (binReader.ReadChar() != '\n') ;
                    }
                    else if (Char.IsWhiteSpace(nextChar))
                    {
                        binReader.ReadChar();
                    }
                    else
                    {
                        switch (headerItemCount)
                        {
                            case 0: //Czytamy nagłówek
                                char[] chars = binReader.ReadChars(2);
                                this.header.PPMNummber = chars[0].ToString() + chars[1].ToString();
                                headerItemCount++;
                                break;
                            case 1: //width
                                this.header.Width = ReadValue(binReader);
                                headerItemCount++;
                                break;
                            case 2: //height
                                this.header.Height = ReadValue(binReader);
                                headerItemCount++;
                                break;
                            case 3: //depth
                                this.header.Depth = ReadValue(binReader);
                                headerItemCount++;
                                break;
                            default:
                                throw new Exception("Błąd nagłówka pliku");
                        }
                    }
                }

                //Czytamy plik
                switch (this.header.PPMNummber)
                {
                    case "P3": // 3 Bajty na pixel
                        this.pixelFormat = PixelFormat.Format24bppRgb;
                        this.bytesPerPixel = 3;
                        break;
                    case "P6":  // 3 Bajty na pixel
                        this.pixelFormat = PixelFormat.Format24bppRgb;
                        this.bytesPerPixel = 3;
                        break;
                    default:
                        throw new Exception("Nieznany typ PPMu " + this.header.PPMNummber);
                }
                this.imageData = new byte[this.header.Width * this.header.Height * this.bytesPerPixel];
                this.stride = this.header.Width * this.bytesPerPixel;
                if (this.header.PPMNummber == "P3") // ASCII Encoding
                {
                    int charsLeft = (int)(binReader.BaseStream.Length - binReader.BaseStream.Position);
                    char[] charData = binReader.ReadChars(charsLeft);  //Zczytujemy caly plik
                    string valueString = string.Empty;
                    index = 0;
                    for (int i = 0; i < charData.Length; i++)
                    {
                        if (Char.IsWhiteSpace(charData[i])) //Jeżeli pusty znak to ignorujemy
                        {
                            if (valueString != string.Empty)
                            {
                                this.imageData[index] = (byte)int.Parse(valueString);
                                valueString = string.Empty;
                                index++;
                            }
                        }
                        else //Dodajemy znak na koniec stringa
                        {
                            valueString += charData[i];
                        }
                    }
                }
                else // binary encoding.
                {
                    int bytesLeft = (int)(binReader.BaseStream.Length - binReader.BaseStream.Position);
                    this.imageData = binReader.ReadBytes(bytesLeft);
                }

                // 3. Zmieniamy kolejność z BGR na RGB
                ReorderBGRtoRGB();

                // 4. Tworzenie bitmapy
                if (this.stride % 4 == 0)
                {
                    this.bitmap = CreateBitMap();
                }
                else
                {
                    this.bitmap = CreateBitmapOffSize();
                }

                // 5. Obracamy obrazek o 180 zeby byl w takiej samej pozycji co orginał
                this.bitmap.RotateFlip(RotateFlipType.Rotate180FlipNone);
            }

            // Jeżeli dojdziemy do końca przed wczytaniem wszystkich przewidzianych wartości to wyrzucamy wyjątek
            catch (EndOfStreamException e)
            {
                Console.WriteLine(e.Message);
                throw new Exception("Error reading the stream! ", e);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception("Error reading the stream! ", ex);
            }
            finally
            {
                binReader.Close();
            }
        }

        //Zamieniamy z BGR na RGB od konca do poczatku obrazka
        private void ReorderBGRtoRGB()
        {
            byte[] tempData = new byte[this.imageData.Length];
            for (int i = 0; i < this.imageData.Length; i++)
            {
                tempData[i] = this.imageData[this.imageData.Length - 1 - i];
            }
            this.imageData = tempData;
        }

        //Czyta znak z pliku
        private int ReadValue(BinaryReader binReader)
        {
            string value = string.Empty;
            while (!Char.IsWhiteSpace((char)binReader.PeekChar()))
            {
                value += binReader.ReadChar().ToString();
            }
            binReader.ReadByte();   // pozbywamy sie białych znaków
            return int.Parse(value);
        }

        //Tworzy bitmape
        private Bitmap CreateBitMap()
        {
            IntPtr pImageData = Marshal.AllocHGlobal(this.imageData.Length);
            Marshal.Copy(this.imageData, 0, pImageData, this.imageData.Length);
            Bitmap bitmap = new Bitmap(this.header.Width, this.header.Height, this.stride, this.pixelFormat, pImageData);
            return bitmap;
        }

        private Bitmap CreateBitmapOffSize()
        {
            Bitmap bitmap = new Bitmap(this.header.Width, this.header.Height, PixelFormat.Format24bppRgb);
            SysColor sysColor = new SysColor();
            int red, green, blue;
            int index;

            for (int x = 0; x < this.header.Width; x++)
            {
                for (int y = 0; y < this.header.Height; y++)
                {
                    index = x + y * this.header.Width;
                    index = 3 * index;
                    if (this.ImageData.Length - 1 < index || this.ImageData.Length - 1 < index + 1 || this.ImageData.Length - 1 < index + 2)
                    {
                        sysColor = SysColor.White;
                    }
                    else {
                        blue = (int)this.imageData[index];
                        green = (int)this.imageData[index + 1];
                        red = (int)this.imageData[index + 2];
                        sysColor = SysColor.FromArgb(red, green, blue);
                    }
                    bitmap.SetPixel(x, y, sysColor);
                }
            }
            return bitmap;
        }
        [Serializable]
        public struct PPMHeader
        {
            private string ppmNumber;
            public string PPMNummber
            {
                get { return ppmNumber; }
                set { ppmNumber = value; }
            }

            private int width;
            public int Width
            {
                get { return width; }
                set { width = value; }
            }

            private int height;
            public int Height
            {
                get { return height; }
                set { height = value; }
            }

            private int depth;
            public int Depth
            {
                get { return depth; }
                set { depth = value; }
            }
        }
    }
}
