﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow: Window
    {
        //Filtry
        private void wygladzajacy_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Filtry.Wybrany(obraz, Filtry.maskWygl, Filtry.normWygl);
        }
        private void medianowy_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Filtry.Medianowy(obraz);
        }
        private void sobel_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Filtry.Wybrany(obraz, Filtry.maskSobel, Filtry.normSobel);
        }
        private void gauss_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Filtry.Wybrany(obraz, Filtry.maskGauss, Filtry.normGauss);
        }
        private void gorny_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Filtry.Wybrany(obraz, Filtry.maskGorny, Filtry.normGorny);
        }

        Dowolny okno = new Dowolny();
        public void dowolny_Click(object sender, RoutedEventArgs e)
        {
            if (okno.closed == true)
                okno = new Dowolny();
            okno.Owner = this;
            okno.Show();
            Filtry.dowolny(obraz, okno, this);
        }
    }
    class Filtry
    {


        static public int normGorny = 1;
        static public int[,] maskGorny = { { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };

        static public int normGauss = 16;
        static public int[,] maskGauss = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };

        static public int normSobel = 1;
        static public int[,] maskSobel = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };

        static public int normWygl = 9;
        static public int[,] maskWygl = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };

        static private byte mediana(byte[] kolor)
        {
            Array.Sort(kolor);
            float size = kolor.Length;
            byte mediana;
            if (size % 2 == 0 && size > 3)
            {
                int index = (int)size / 2;
                mediana = (byte)((kolor[index] + kolor[index - 1]) / 2);
            }
            else
            {
                mediana = kolor[(int)Math.Floor(size / 2)];
            }
            return mediana;
        }

        static public BitmapSource Medianowy(Obraz obraz)
        {
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            int a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    stary2D[i, j] = nowyObraz[a];
                    nowy2D[i, j] = nowyObraz[a++];
                }

            }
            byte[] r = new byte[9], g = new byte[9], b = new byte[9];
            int index;
            for (int y = 0; y < obraz.pixelHeight; ++y)
            {
                for (int x = 0; x < obraz.stride; x += 4)
                {
                    index = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 12; j += 4)
                        {
                            int ny = y - 1 + i;
                            int nx = x - 4 + j;
                            if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                            {
                                r[index] = 255;
                                g[index] = 255;
                                b[index++] = 255;
                            }
                            else
                            {
                                r[index] = stary2D[ny, nx + 2];
                                g[index] = stary2D[ny, nx + 1];
                                b[index++] = stary2D[ny, nx];
                            }
                        }
                    }
                    nowy2D[y, x] = mediana(b);
                    nowy2D[y, x + 1] = mediana(g);
                    nowy2D[y, x + 2] = mediana(r);
                }
            }

            a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    nowyObraz[a++] = nowy2D[i, j];
                }

            }
            return obraz.KlonujObraz(nowyObraz);
                
        }

        static public BitmapSource Wybrany(Obraz obraz, int[,] mask, int norm)
        {
            byte[] nowyObraz = obraz.PobierzDaneRGB();
            byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
            byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
            int a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    stary2D[i, j] = nowyObraz[a];
                    nowy2D[i, j] = nowyObraz[a++];
                }

            }
            float r = 0, g = 0, b = 0;
            for (int y = 0; y < obraz.pixelHeight; ++y)
            {
                for (int x = 0; x < obraz.stride; x += 4)
                {
                    r = 0;
                    g = 0;
                    b = 0;
                    for (int i = 0; i < 3; ++i)
                    {
                        for (int j = 0; j < 12; j += 4)
                        {
                            int ny = y - 1 + i;
                            int nx = x - 4 + j;
                            if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                            {
                                r += 255;
                                g += 255;
                                b += 255;
                            }
                            else
                            {
                                r += mask[i, j / 4] * stary2D[ny, nx + 2];
                                g += mask[i, j / 4] * stary2D[ny, nx + 1];
                                b += mask[i, j / 4] * stary2D[ny, nx];
                            }
                        }
                    }
                    r /= norm;
                    g /= norm;
                    b /= norm;
                    nowy2D[y, x] = (byte)Math.Min(255, Math.Max(0, b));
                    nowy2D[y, x + 1] = (byte)Math.Min(255, Math.Max(0, g));
                    nowy2D[y, x + 2] = (byte)Math.Min(255, Math.Max(0, r));
                }
            }
            a = 0;
            for (int i = 0; i < obraz.pixelHeight; ++i)
            {
                for (int j = 0; j < obraz.stride; ++j)
                {
                    nowyObraz[a++] = nowy2D[i, j];
                }

            }
            return obraz.KlonujObraz(nowyObraz);

        }

        static public void dowolny(Obraz obraz, Dowolny okno, MainWindow window)
        {

            bool proceed = okno.proceed;
            if (proceed)
            {
                float norm;
                norm = okno.norm;
                int row = okno.row;
                int col = okno.col;
                byte[] nowyObraz = obraz.PobierzDaneRGB();
                byte[,] stary2D = new byte[obraz.pixelHeight, obraz.stride];
                byte[,] nowy2D = new byte[obraz.pixelHeight, obraz.stride];
                float[,] mask = okno.mask;
                int a = 0;
                for (int i = 0; i < obraz.pixelHeight; ++i)
                {
                    for (int j = 0; j < obraz.stride; ++j)
                    {
                        stary2D[i, j] = nowyObraz[a];
                        nowy2D[i, j] = nowyObraz[a++];
                    }

                }
                float r = 0, g = 0, b = 0;
                for (int y = 0; y < obraz.pixelHeight; ++y)
                {
                    for (int x = 0; x < obraz.stride; x += 4)
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                        for (int i = 0; i < row; ++i)
                        {
                            for (int j = 0; j < col * 4; j += 4)
                            {
                                int ny = y - (int)Math.Floor((float)row / 2f) + i;
                                int nx = x - ((int)Math.Floor((float)col / 2f)) * 4 + j;
                                if (nx < 0 || ny < 0 || nx >= obraz.stride || ny >= obraz.pixelHeight)
                                //if (nx < 0 || nx < size * ny || nx > (x+4) * (y+1) * ny)
                                {
                                    r += 255;
                                    g += 255;
                                    b += 255;
                                }
                                else
                                {
                                    r += mask[i, j / 4] * stary2D[ny, nx + 2];
                                    g += mask[i, j / 4] * stary2D[ny, nx + 1];
                                    b += mask[i, j / 4] * stary2D[ny, nx];
                                }
                            }
                        }
                        r /= norm;
                        g /= norm;
                        b /= norm;
                        nowy2D[y, x] = (byte)Math.Min(255, Math.Max(0, b));
                        nowy2D[y, x + 1] = (byte)Math.Min(255, Math.Max(0, g));
                        nowy2D[y, x + 2] = (byte)Math.Min(255, Math.Max(0, r));
                    }
                }
                a = 0;
                for (int i = 0; i < obraz.pixelHeight; ++i)
                {
                    for (int j = 0; j < obraz.stride; ++j)
                    {
                        nowyObraz[a++] = nowy2D[i, j];
                    }

                }
                window.kopia.Source = obraz.KlonujObraz(nowyObraz);
            }

        }
    }
}
