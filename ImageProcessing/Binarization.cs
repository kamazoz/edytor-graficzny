﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {
        //Binaryzacja
        private void dowolnaBin_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.Dowolny(obraz, prog);
        }

        private void psc_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.ProcentCzerni(obraz, (float)prog);
        }

        private void sis_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.IteratywnaSrednia(obraz, progTB);
        }

        //Metoda Puna entropii szukamy wartosci maksymalnej entropi sprawdzaja kazdy prog po binaryzacji
        private void se_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.Entropia(obraz, progTB);
        }

        private void bm_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.BladMinimalny(obraz, progTB);
        }
        private void mrbm_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Binarization.RozmytyBladMinimalny(obraz, progTB);
        }
        private void progTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            long v;
            bool parsed = long.TryParse(progTB.Text, out v);
            if (parsed)
                prog = Math.Max(0, Math.Min(255, v));
            else
                prog = 0;
        }
    }
    class Binarization
    {
        static public BitmapSource Dowolny(Obraz obraz, double prog)
        {
            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();

            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                byte nValue = 0;
                if (gray >= prog)
                    nValue = 255;
                newImg[i] = nValue;
                newImg[i + 1] = nValue;
                newImg[i + 2] = nValue;
            }

            return obraz.KlonujObraz(newImg);
        }

        static public BitmapSource ProcentCzerni(Obraz obraz, float percent)
        {
            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();
            float N = obraz.pixelWidth * obraz.pixelHeight;

            percent = Math.Max(0, Math.Min(100, percent));
            float threshold = N * percent / 100;
            float total = 0;
            float[] tab = new float[256];

            int a = 0;
            for (int i = 0; i < 255; i++)
            {
                if (total < threshold)
                {
                    tab[a++] = i;
                    total += obraz.hist[i];
                }
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                int index = gray;

                byte nValue = 255;
                if (tab[gray] == gray)
                {
                    nValue = 0;
                }
                if (percent == 100)
                {
                    newImg[i] = newImg[i + 1] = newImg[i + 2] = 0;
                }
                else if (percent == 0)
                {
                    newImg[i] = newImg[i + 1] = newImg[i + 2] = 255;
                }
                else
                {
                    newImg[i] = nValue;
                    newImg[i + 1] = nValue;
                    newImg[i + 2] = nValue;
                }
            }
            return obraz.KlonujObraz(newImg);
        }

        static public BitmapSource IteratywnaSrednia(Obraz obraz, TextBox tb)
        {
            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();
            float TWhite = 0;
            float TBlack = 0;
            float threshold = 0;
            float prevThreshold;
            float total = obraz.pixelHeight * obraz.pixelWidth;
            float Winde = 0;
            float Wmian = 0;
            float Binde = 0;
            float Bmian = 0;
            for (int j = 0; j < 255; ++j)
            {
                TBlack = 0;
                TWhite = 0;
                Winde = 0;
                Wmian = 0;
                Binde = 0;
                Bmian = 0;
                for (int i = 0; i < obraz.dataLength; i += 4)
                {
                    int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                    int index = gray;
                    if (gray >= threshold)
                    {
                        Winde += index * obraz.hist[index];
                        Wmian += obraz.hist[index];
                    }
                    else
                    {
                        Binde += index * obraz.hist[index];
                        Bmian += obraz.hist[index];
                    }
                }
                Wmian *= 2;
                Bmian *= 2;
                if (Bmian == 0) Bmian = 2;
                if (Wmian == 0) Wmian = 2;
                TWhite = Winde / Wmian;
                TBlack = Binde / Bmian;
                prevThreshold = threshold;
                threshold = TBlack + TWhite;
                if (prevThreshold == threshold) break;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                int index = gray;
                byte nValue = 0;
                if (gray >= threshold)
                {
                    nValue = 255;
                }
                newImg[i] = nValue;
                newImg[i + 1] = nValue;
                newImg[i + 2] = nValue;
            }
            int th = (int)Math.Round(threshold);
            tb.Text = th.ToString();
            return obraz.KlonujObraz(newImg);

        }

        //Metoda Puna entropii szukamy wartosci maksymalnej entropi sprawdzaja kazdy prog po binaryzacji
        static public BitmapSource Entropia(Obraz obraz, TextBox tb)
        {

            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();
            double entropy = 0;
            double total = obraz.pixelHeight * obraz.pixelWidth;
            double[] histogram = new double[2];
            double maxEntropy = -1;
            double threshold = 0;
            for (int a = 0; a < 256; ++a)
            {
                entropy = 0;
                histogram[0] = 0;
                histogram[1] = 0;
                for (int i = 0; i < obraz.dataLength; i += 4)
                {
                    int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                    if (gray >= a)
                        ++histogram[0];
                }
                histogram[1] = total - histogram[0];
                for (int i = 0; i < 2; ++i)
                {
                    double p = histogram[i] / total;
                    if (p == 0)
                        entropy += 0;
                    else
                        entropy += p * Math.Log(p, 2);
                }
                entropy = -entropy;
                if (entropy >= maxEntropy)
                {
                    threshold = a;
                    maxEntropy = entropy;
                }
                else
                    break;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                byte nValue = 0;
                if (gray >= threshold)
                {
                    nValue = 255;
                }
                newImg[i] = nValue;
                newImg[i + 1] = nValue;
                newImg[i + 2] = nValue;
            }
            int th = (int)threshold;
            tb.Text = th.ToString();
            return obraz.KlonujObraz(newImg);
        }

        static public BitmapSource BladMinimalny(Obraz obraz, TextBox tb)
        {
            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();
            double[] test = new double[256];
            double first = 0;
            double second = 0;
            double exp = Math.Exp(1);
            double power = 0;
            double mean = 0;
            double variance = 0;
            double mianownik = 0;
            double threshold = 0;
            for (int i = 0; i < 256; ++i)
            {
                mean = mikro0(i, obraz);
                variance = variance0(i, mean, obraz);
                mianownik = 2 * variance;
                if (mianownik == 0) { test[i] = 0; continue; }
                power = -(((i - mean) * (i - mean)) / mianownik);
                mianownik = (Math.Sqrt(variance * 2 * Math.PI));
                if (mianownik == 0) { test[i] = 0; continue; }
                first = Math.Pow(exp, power) / mianownik;


                mean = mikro1(i, obraz);
                variance = variance1(i, mean, obraz);
                mianownik = 2 * variance;
                if (mianownik == 0) { test[i] = 0; continue; }
                power = -(((i - mean) * (i - mean)) / mianownik);
                mianownik = (Math.Sqrt(variance * 2 * Math.PI));
                if (mianownik == 0) { test[i] = 0; continue; }
                second = Math.Pow(exp, power) / mianownik;
                test[i] = first + second;
            }
            threshold = Array.IndexOf(test, test.Max());

            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                byte nValue = 0;
                if (gray >= threshold)
                {
                    nValue = 255;
                }
                newImg[i] = nValue;
                newImg[i + 1] = nValue;
                newImg[i + 2] = nValue;
            }
            int th = (int)threshold;
            tb.Text = th.ToString();
            return obraz.KlonujObraz(newImg);
        }
        static private double variance0(int t, double mean, Obraz obraz)
        {
            double variance = 0;
            double total = obraz.pixelHeight * obraz.pixelWidth;
            for (int i = 0; i <= t; ++i)
            {
                variance += (t - mean) * (t - mean) * obraz.hist[i];
            }
            variance /= total;
            return variance;

        }
        static private double variance1(int t, double mean, Obraz obraz)
        {
            double variance = 0;
            double total = obraz.pixelHeight * obraz.pixelWidth;
            for (int i = t + 1; i < 256; ++i)
            {
                variance += (t - mean) * (t - mean) * obraz.hist[i];
            }
            variance /= total;
            return variance;
        }
        static private double uFun(double x, int t, Obraz obraz)
        {
            double u = 0;
            if (x <= t)
            {
                u = 1 / (1 + (Math.Abs(x - mikro0(t, obraz))) / 255);
            }
            else
            {
                u = 1 / (1 + (Math.Abs(x - mikro1(t, obraz))) / 255);
            }
            return u;
        }
        static private double mikro0(int t, Obraz obraz)
        {
            double licznik = 0;
            double mianownik = 0;
            for (int i = 0; i <= t; ++i)
            {
                licznik += obraz.hist[i] * i;
                mianownik += obraz.hist[i];
            }
            if (mianownik == 0) mianownik = 1;
            return licznik / mianownik;
        }
        static private double mikro1(int t, Obraz obraz)
        {
            double licznik = 0;
            double mianownik = 0;
            for (int i = t + 1; i < 256; ++i)
            {
                licznik += obraz.hist[i] * i;
                mianownik += obraz.hist[i];
            }
            if (mianownik == 0) mianownik = 1;
            return licznik / mianownik;
        }
        static private double Hf(double x)
        {
            if (x == 1) return 0;
            return -x * Math.Log(x, 2) - (1 - x) * Math.Log(1 - x, 2);
        }
        static private double E(int t, Obraz obraz)
        {
            double Esum = 0;
            for (int i = 0; i < 255; ++i)
            {
                Esum += Hf(uFun(i, t, obraz)) * obraz.hist[i];
            }
            Esum = (Esum / ((obraz.pixelHeight * obraz.pixelWidth)));
            return Esum;
        }

        static public BitmapSource RozmytyBladMinimalny(Obraz obraz, TextBox tb)
        {
            byte[] newImg = obraz.PobierzDaneRGB();
            byte[] bitImg = obraz.PobierzDaneRGB();

            double Esum;
            double minEsum = 1;
            int threshold = 0;
            for (int j = 0; j < 256; ++j)
            {

                Esum = E(j, obraz);
                if (Esum <= minEsum)
                {
                    minEsum = Esum;
                    threshold = j;
                }
                else
                    break;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                int index = gray;
                byte nValue = 0;
                if (gray >= threshold)
                {
                    nValue = 255;
                }
                newImg[i] = nValue;
                newImg[i + 1] = nValue;
                newImg[i + 2] = nValue;
            }
            int th = (int)threshold;
            tb.Text = th.ToString();
            return obraz.KlonujObraz(newImg);
        }

        /*
        private void progTB_TextChanged(object sender, TextChangedEventArgs e)
        {
            long v;
            bool parsed = long.TryParse(progTB.Text, out v);
            if (parsed)
                prog = v;
            else
                prog = 0;
        }

        private void progPSC_TextChanged(object sender, TextChangedEventArgs e)
        {
            long v;
            bool parsed = long.TryParse(progPSC.Text, out v);
            if (parsed && v < 101 && v >= 0)
                percent = v;
            else
                percent = 0;
        }
        */
    }
}
