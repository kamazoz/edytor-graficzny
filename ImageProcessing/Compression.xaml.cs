﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for Compression.xaml
    /// </summary>
    public partial class Compression : Window
    {
        public bool save = false;
        public Int64 compression = 0;
        public Compression()
        {
            InitializeComponent();

        }

        private void zapisz(object sender, RoutedEventArgs e)
        {
            save = true;
            compression = (Int64)slider.Value;
            Close();
        }
        private void anuluj(object sender, RoutedEventArgs e)
        {
            save = false;
            Close();
        }
    }
}
