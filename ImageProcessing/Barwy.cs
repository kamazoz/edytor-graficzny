﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {

        //Barwy
        RgbToCmyk rgbToCmyk = new RgbToCmyk();
        private void Rgbtocmyk(object sender, RoutedEventArgs e)
        {
            if (rgbToCmyk.closed == true)
                rgbToCmyk = new RgbToCmyk();
            rgbToCmyk.Owner = this;
            rgbToCmyk.Show();
        }
        HSV hsv = new HSV();
        private void ConeHSV(object sender, RoutedEventArgs e)
        {
            if (hsv.closed == true)
                hsv = new HSV();
            hsv.Owner = this;
            hsv.Show();
        }
        Cube Cubergb = new Cube();
        private void CubeRGB(object sender, RoutedEventArgs e)
        {
            if (Cubergb.closed == true)
                Cubergb = new Cube();
            Cubergb.Owner = this;
            Cubergb.Show();
        }


        //RGB
        private void Red_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Red(obraz);
        }
        private void Green_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Green(obraz);
        }
        private void Blue_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Blue(obraz);
        }
        private void Gray_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.GraySecond(obraz);
        }
        private void Cmyk_To_Rgb_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.CmykToRGB(obraz);
            RGB.Visibility = Visibility.Hidden;
            CMYK.Visibility = Visibility.Visible;
        }


        //CMYK
        private void Cyan_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Cyan(obraz);
        }
        private void Magenta_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Magenta(obraz);
        }
        private void Yellow_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Yellow(obraz);
        }
        private void C_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.C(obraz);
        }
        private void M_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.M(obraz);
        }
        private void Y_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Y(obraz);
        }
        private void K_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.K(obraz);
        }
        private void RGB_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = obraz.original;
            RGB.Visibility = Visibility.Visible;
            CMYK.Visibility = Visibility.Hidden;
        }
    }
    class Barwy
    {
        static public BitmapSource Red(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for(int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = 0;
                newPicture[i + 1] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Blue(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i + 1] = 0;
                newPicture[i + 2] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Green(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = 0;
                newPicture[i + 2] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource GraySecond(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double grayVal = 0.114 * newPicture[i] + 0.587 * newPicture[i + 1] + 0.299 * newPicture[i + 2];
                newPicture[i] = (byte)grayVal;
                newPicture[i + 1] = (byte)grayVal;
                newPicture[i + 2] = (byte)grayVal;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Gray(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double grayVal = ((double)newPicture[i] + (double)newPicture[i + 1] + (double)newPicture[i + 2])/3.0;
                newPicture[i] = (byte)grayVal;
                newPicture[i + 1] = (byte)grayVal;
                newPicture[i + 2] = (byte)grayVal;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Cyan(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i + 2] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Magenta(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i + 1] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource Yellow(Obraz obraz)
        {
            byte[] newPicture = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = 0;
            }
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource CmykToRGB(Obraz obraz)
        {
            float[] newCmyk = CmykClass.RgbToCmyk(obraz.PobierzDaneRGB());
            byte[] newPicture = CmykClass.CmykToRgb(newCmyk);
            return obraz.KlonujObraz(newPicture);
        }
        static public BitmapSource C(Obraz obraz)
        {
            float[] newCmyk = CmykClass.RgbToCmyk(obraz.PobierzDaneRGB());
            byte[] newPicture = new byte[obraz.dataLength];
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = newPicture[i + 1] = newPicture[i + 2] = (byte)Math.Min(255.0, (255 * (1.0 - newCmyk[i + (int)CMYK.Cyan])));
                newPicture[i + 3] = 255;
            }   
            return obraz.KlonujObraz(newPicture);
        }

        static public BitmapSource M(Obraz obraz)
        {
            float[] newCmyk = CmykClass.RgbToCmyk(obraz.PobierzDaneRGB());
            byte[] newPicture = new byte[obraz.dataLength];
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = newPicture[i + 1] = newPicture[i + 2] = (byte)Math.Min(255.0, (255 * (1.0 - newCmyk[i + (int)CMYK.Magenta])));
                newPicture[i + 3] = 255;
            }
            return obraz.KlonujObraz(newPicture);
        }

        static public BitmapSource Y(Obraz obraz)
        {
            float[] newCmyk = CmykClass.RgbToCmyk(obraz.PobierzDaneRGB());
            byte[] newPicture = new byte[obraz.dataLength];
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = newPicture[i + 1] = newPicture[i + 2] = (byte)Math.Min(255.0, (255 * (1.0 - newCmyk[i + (int)CMYK.Yellow])));
                newPicture[i + 3] = 255;
            }
            return obraz.KlonujObraz(newPicture);
        }

        static public BitmapSource K(Obraz obraz)
        {
            float[] newCmyk = CmykClass.RgbToCmyk(obraz.PobierzDaneRGB());
            byte[] newPicture = new byte[obraz.dataLength];
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                newPicture[i] = newPicture[i + 1] = newPicture[i + 2] = (byte)Math.Min(255.0, (255 * (1.0 - newCmyk[i + (int)CMYK.Black])));
                newPicture[i + 3] = 255;
            }
            return obraz.KlonujObraz(newPicture);
        }
    }
   
}
