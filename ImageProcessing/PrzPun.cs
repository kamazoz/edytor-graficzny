﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {
        //Punkty
        private void wyliczKolory(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (obraz == null) return;
            byte[] img = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double sv = slider.Value;
                double a1 = (blueSliderMno.Value * ((double)img[i + 0] - 255.0 / 2.0) + 255.0 / 2.0);
                double a2 = (greenSliderMno.Value * ((double)img[i + 1] - 255.0 / 2.0) + 255.0 / 2.0);
                double a3 = (redSliderMno.Value * ((double)img[i + 2] - 255.0 / 2.0) + 255.0 / 2.0);
                img[i] = (byte)Math.Max(0, Math.Min(255, a1 + blueSlider.Value));
                img[i + 1] = (byte)Math.Max(0, Math.Min(255, a2 + greenSlider.Value));
                img[i + 2] = (byte)Math.Max(0, Math.Min(255, a3 + redSlider.Value));
            }
            kopia.Source = obraz.KlonujObraz(img);
        }
        private void zmienTrybDodMno(object sender, RoutedEventArgs e)
        {
            if (redSlider.Visibility == Visibility.Visible)
            {
                redSlider.Visibility = Visibility.Hidden;
                greenSlider.Visibility = Visibility.Hidden;
                blueSlider.Visibility = Visibility.Hidden;
                redSliderMno.Visibility = Visibility.Visible;
                greenSliderMno.Visibility = Visibility.Visible;
                blueSliderMno.Visibility = Visibility.Visible;

                redLabel.Visibility = Visibility.Hidden;
                greenLabel.Visibility = Visibility.Hidden;
                blueLabel.Visibility = Visibility.Hidden;
                redLabelMno.Visibility = Visibility.Visible;
                greenLabelMno.Visibility = Visibility.Visible;
                blueLabelMno.Visibility = Visibility.Visible;
            }
            else
            {
                redSliderMno.Visibility = Visibility.Hidden;
                greenSliderMno.Visibility = Visibility.Hidden;
                blueSliderMno.Visibility = Visibility.Hidden;
                redSlider.Visibility = Visibility.Visible;
                greenSlider.Visibility = Visibility.Visible;
                blueSlider.Visibility = Visibility.Visible;

                redLabel.Visibility = Visibility.Visible;
                greenLabel.Visibility = Visibility.Visible;
                blueLabel.Visibility = Visibility.Visible;
                redLabelMno.Visibility = Visibility.Hidden;
                greenLabelMno.Visibility = Visibility.Hidden;
                blueLabelMno.Visibility = Visibility.Hidden;
            }

            redSlider.Value = 0;
            redSliderMno.Value = 1;
            greenSlider.Value = 0;
            greenSliderMno.Value = 1;
            blueSlider.Value = 0;
            blueSliderMno.Value = 1;
        }
        private void pojedynczeKol(object sender, RoutedEventArgs e)
        {
            Punkty.Visibility = Visibility.Hidden;
            pojedynczeKolory.Visibility = Visibility.Visible;
        }
        private void buttonGamma_Click(object sender, RoutedEventArgs e)
        {
            sliderKon.Visibility = Visibility.Hidden;
            slider.Visibility = Visibility.Visible;
            labelKon.Visibility = Visibility.Hidden;
            label.Visibility = Visibility.Visible;
            slider.Value = 0;
            sliderKon.Value = 1;

        }
        private void buttonKontrast_Click(object sender, RoutedEventArgs e)
        {
            sliderKon.Visibility = Visibility.Visible;
            slider.Visibility = Visibility.Hidden;
            labelKon.Visibility = Visibility.Visible;
            label.Visibility = Visibility.Hidden;
            slider.Value = 0;
            sliderKon.Value = 1;
        }


        private void buttonGray_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.Gray(obraz);
        }
        private void buttonGray2_Click(object sender, RoutedEventArgs e)
        {
            kopia.Source = Barwy.GraySecond(obraz);
        }
        private void slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (obraz == null) return;
            kopia.Source = PrzPun.Gamma(obraz, slider);
        }
        private void sliderKon_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (obraz == null) return;
            kopia.Source = PrzPun.Kontrast(obraz, sliderKon);
        }
    }
    class PrzPun
    {
        static public BitmapSource Kontrast(Obraz obraz, Slider slider)
        {
            
            byte[] img = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double sv = slider.Value;
                double a1 = (sv * ((double)img[i + 0] - 255.0 / 2.0) + 255.0 / 2.0);
                double a2 = (sv * ((double)img[i + 1] - 255.0 / 2.0) + 255.0 / 2.0);
                double a3 = (sv * ((double)img[i + 2] - 255.0 / 2.0) + 255.0 / 2.0);
                img[i] = (byte)Math.Max(0, Math.Min(255, a1));
                img[i + 1] = (byte)Math.Max(0, Math.Min(255, a2));
                img[i + 2] = (byte)Math.Max(0, Math.Min(255, a3));
            }
            return obraz.KlonujObraz(img);
        }
        static public BitmapSource Gamma(Obraz obraz, Slider slider)
        {
            byte[] img = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double sv = slider.Value;
                img[i] = (byte)Math.Max(0, Math.Min(255, sv + img[i]));
                img[i + 1] = (byte)Math.Max(0, Math.Min(255, sv + img[i + 1]));
                img[i + 2] = (byte)Math.Max(0, Math.Min(255, sv + img[i + 2]));
            }
            return obraz.KlonujObraz(img);
        }
        static public BitmapSource Gray(Obraz obraz, Slider slider)
        {
            byte[] img = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                double sv = slider.Value;
                img[i] = img[i + 1] = img[i + 2] = (byte)Math.Max(0, Math.Min(255, sv + img[i]));
            }
            return obraz.KlonujObraz(img);
        }
    }
}
