﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {
        //Histogramy
        private void equalizing(object sender, RoutedEventArgs e)
        {
            kopia.Source = Histogram.equalizing(obraz);
        }
        private void normalize(object sender, RoutedEventArgs e)
        {
            kopia.Source = Histogram.normalize_Click(obraz);
        }
        private void histogramR(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            ShowSelectedImage(Histogram.drawHistogramRed(o), orginal);
            jasnosc.Visibility = Visibility.Visible;
        }
        private void histogramG(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            ShowSelectedImage(Histogram.drawHistogramGreen(o), orginal);
            jasnosc.Visibility = Visibility.Visible;
        }
        private void histogramB(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            ShowSelectedImage(Histogram.drawHistogramBlue(o), orginal);
            jasnosc.Visibility = Visibility.Visible;
        }
        private void histogram(object sender, RoutedEventArgs e)
        {
            Obraz o = new Obraz(kopia.Source as BitmapSource);
            ShowSelectedImage(Histogram.drawHistogram(o), orginal);
            jasnosc.Visibility = Visibility.Visible;
        }
    }
    class Histogram
    {
        static public float[] LUT(float[] histogram, long numPixel)
        {
            float[] histo = new float[256];
            histo[0] = histogram[0] * histogram.Length / numPixel;
            float prev = histogram[0];

            for (int i = 1; i < histo.Length; i++)
            {
                prev += histogram[i];
                histo[i] = prev * histogram.Length / numPixel;
            }

            return histo;

        }
        static public BitmapSource normalize_Click(Obraz obraz)
        {
            int maxR = 0;
            int minR = 255;
            int maxG = 0;
            int minG = 255;
            int maxB = 0;
            int minB = 255;
            byte[] newByte = obraz.PobierzDaneRGB();
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                if (newByte[i + 2] > maxR)
                    maxR = newByte[i + 2];

                if (newByte[i + 2] < minR)
                    minR = newByte[i + 2];

                if (newByte[i + 1] > maxG)
                    maxG = newByte[i + 1];

                if (newByte[i + 1] < minG)
                    minG = newByte[i + 1];

                if (newByte[i] > maxB)
                    maxB = newByte[i];

                if (newByte[i] < minB)
                    minB = newByte[i];
            }

            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int r = (int)((((double)newByte[i + 2] - minR) / (maxR - minR)) * 255);
                int g = (int)((((double)newByte[i + 1] - minG) / (maxG - minG)) * 255);
                int b = (int)((((double)newByte[i] - minB) / (maxB - minB)) * 255);
                newByte[i + 2] = (byte)r;
                newByte[i + 1] = (byte)g;
                newByte[i] = (byte)b;
            }
            return obraz.KlonujObraz(newByte);
        }
        static public BitmapSource equalizing(Obraz obraz)
        {
            byte[] newImg = new byte[obraz.dataLength];
            byte[] bitImg = obraz.PobierzDaneRGB();

            float[] lut = LUT(obraz.hist, obraz.pixelHeight * obraz.pixelWidth);
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((bitImg[i] + bitImg[i + 1] + bitImg[i + 2]) / 3.0);
                int index = (int)bitImg[i];
                byte nValue = (byte)lut[index];
                if (lut[index] > 255)
                    nValue = 255;
                newImg[i] = newImg[i + 1] = newImg[i + 2] = newImg[i + 3] = nValue;
            }
            return obraz.KlonujObraz(newImg);
        }

        static private Bitmap draw(float[] h, float max, Pen kol)
        {
            int heightOffset = 255;
            int widthOffset = 255;

            Bitmap img = new Bitmap(widthOffset, heightOffset + 10);
            for (int i = 0; i < img.Height; ++i)
            {
                for (int j = 0; j < img.Width; ++j)
                {
                    Color col = Color.White;
                    img.SetPixel(j, i, col);
                }
            }
            using (Graphics g = Graphics.FromImage(img))
            {
                for (int i = 0; i < h.Length; i++)
                {
                    float pct = heightOffset * h[i] / max;
                    g.DrawLine(kol,
                        new System.Drawing.Point(i, img.Height - 5),
                        new System.Drawing.Point(i, img.Height - 5 - (int)(pct))
                        );
                }
            }
            return img;
        }

        static public Bitmap drawHistogram(Obraz obraz)
        {
            byte[] rgb = obraz.PobierzDaneRGB();
            for (int i = 0; i < 256; ++i)
            {
                obraz.hist[i] = 0;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int gray = (int)((rgb[i] + rgb[i + 1] + rgb[i + 2]) / 3.0);
                ++obraz.hist[gray];
            }

            float max = -1;
            float min = 256;

            for (int i = 0; i < obraz.hist.Length; i++)
            {
                if (obraz.hist[i] > max)
                    max = obraz.hist[i];

                if (obraz.hist[i] < min)
                    min = obraz.hist[i];
            }
            return draw(obraz.hist, max, Pens.Gray);
        }
        
        static public Bitmap drawHistogramRed(Obraz obraz)
        {
            byte[] rgb = obraz.PobierzDaneRGB();
            float[] histR = new float[256];
            for (int i = 0; i < 256; ++i)
            {
                histR[i] = 0;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int red = (int)rgb[i + 2];
                ++histR[red];
            }
            float max = -1;
            float min = 256;

            for (int i = 0; i < histR.Length; i++)
            {
                if (histR[i] > max)
                    max = histR[i];

                if (histR[i] < min)
                    min = histR[i];
            }

            return draw(histR, max, Pens.Red);
        }
        static public Bitmap drawHistogramGreen(Obraz obraz)
        {
            byte[] rgb = obraz.PobierzDaneRGB();
            float[] histG = new float[256];
            for (int i = 0; i < 256; ++i)
            {
                histG[i] = 0;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int green = (int)rgb[i + 1];
                ++histG[green];
            }

            float max = -1;
            float min = 256;

            for (int i = 0; i < histG.Length; i++)
            {
                if (histG[i] > max)
                    max = histG[i];

                if (histG[i] < min)
                    min = histG[i];
            }
            return draw(histG, max, Pens.Green);
        }
        static public Bitmap drawHistogramBlue(Obraz obraz)
        {
            byte[] rgb = obraz.PobierzDaneRGB();
            float[] histB = new float[256];
            for (int i = 0; i < 256; ++i)
            {
                histB[i] = 0;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                int blue = (int)rgb[i];
                ++histB[blue];
            }
            float max = -1;
            float min = 256;

            for (int i = 0; i < histB.Length; i++)
            {
                if (histB[i] > max)
                    max = histB[i];

                if (histB[i] < min)
                    min = histB[i];
            }

            return draw(histB, max, Pens.Blue);
        }
    }
}
