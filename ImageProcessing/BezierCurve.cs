﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {
        //Bezier
        private void addEvents()
        {
            for (int i = 0; i < listaBeziera.Last().pointsEll.Length; ++i)
                listaBeziera.Last().pointsEll[i].MouseDown += p_down;
            listaBeziera.Last().addToCanvas(imgCanva);
        }
        private void p_down(object sender, MouseButtonEventArgs e)
        {
            if (bezier == null) return;
            foreach (BezierCurve b in listaBeziera)
            {
                for (int i = 0; i < b.pointsEll.Length; ++i)
                {
                    if (sender.Equals(b.pointsEll[i]))
                    {
                        b.pressed[i] = true;
                        break;
                    }
                }
            }

        }
    }
    class BezierCurve
    {

        public Polyline polyline;
        public Ellipse[] pointsEll;
        public Line[] lines;
        public Point[] points;
        public bool[] pressed;
        public BezierCurve()
        {
            
        }

        public BezierCurve(Point[] p)
        {
            points = p;
            pressed = new bool[p.Length];
            pointsEll = new Ellipse[p.Length];
            lines = new Line[p.Length - 1];
            polyline = new Polyline
            {
                StrokeThickness = 2,
                Stroke = Brushes.Black,
                IsEnabled = false
            };
            double t = 0;
            while (t <= 1.00099)
            {
                polyline.Points.Add(multicurve(points, t));
                t += 0.001;
            }
            for (int i = 0; i < points.Length; ++i)
            {
                pressed[i] = false;
                pointsEll[i] = new Ellipse
                {
                    Stroke = Brushes.Black,
                    StrokeThickness = 2,
                    Width = 20,
                    Height = 20,
                    Fill = Brushes.Blue,
                    Opacity = 0.4

                };
                Canvas.SetTop(pointsEll[i], points[i].Y - 10);
                Canvas.SetLeft(pointsEll[i], points[i].X - 10);
            }
            for (int i = 0; i < lines.Length; ++i)
            {
                lines[i] = new Line
                {
                    Stroke = Brushes.Gray,
                    StrokeThickness = 2,
                    StrokeDashArray = { 2, 4 }
                };
                lines[i].X1 = points[i].X;
                lines[i].Y1 = points[i].Y;
                lines[i].X2 = points[i + 1].X;
                lines[i].Y2 = points[i + 1].Y;
            }


        }

        private void drawLines()
        {
            for (int i = 0; i < points.Length - 1; ++i)
            {
                lines[i].X1 = points[i].X;
                lines[i].Y1 = points[i].Y;
                lines[i].X2 = points[i + 1].X;
                lines[i].Y2 = points[i + 1].Y;
            }
        }

        public void addToCanvas(Canvas canva)
        {
            canva.Children.Add(polyline);
            for (int i = 0; i < lines.Length; ++i)
                canva.Children.Add(lines[i]);
            for (int i = 0; i < pointsEll.Length; ++i)
            {
                canva.Children.Add(pointsEll[i]);
            }
        }
        public void removeFromCanvas(Canvas canva)
        {
            canva.Children.Remove(polyline);
            for (int i = 0; i < lines.Length; ++i)
                canva.Children.Remove(lines[i]);
            for (int i = 0; i < pointsEll.Length; ++i)
            {
                canva.Children.Remove(pointsEll[i]);
            }
        }
        public void hidePoints(Canvas canva)
        {

            for (int i = 0; i < lines.Length; ++i)
            {
                canva.Children.Remove(lines[i]);
                lines[i].Visibility = Visibility.Hidden;
                canva.Children.Add(lines[i]);
            }
            for (int i = 0; i < pointsEll.Length; ++i)
            {
                canva.Children.Remove(pointsEll[i]);
                pointsEll[i].Visibility = Visibility.Hidden;
                canva.Children.Add(pointsEll[i]);
            }
        }
        public void showPoints(Canvas canva)
        {
            for (int i = 0; i < lines.Length; ++i)
            {
                canva.Children.Remove(lines[i]);
                lines[i].Visibility = Visibility.Visible;
                canva.Children.Add(lines[i]);
            }
            for (int i = 0; i < pointsEll.Length; ++i)
            {
                canva.Children.Remove(pointsEll[i]);
                pointsEll[i].Visibility = Visibility.Visible;
                canva.Children.Add(pointsEll[i]);
            }
        }
        private double Newton(int n, int k)
        {
            long Wynik = 1;
            for (int i = 1; i <= k; ++i)
            {
                Wynik = Wynik * (n - i + 1) / i;
            }
            return Wynik;
        }

        private Point multicurve(Point[] points, double t)
        {
            int power = points.Length;
            Point p = new Point(0, 0);
            for (int i = 0; i < power; ++i)
            {
                p.X += Newton(power - 1, i) * Math.Pow(t, i) * Math.Pow((1 - t), power - 1 - i) * points[i].X;
                p.Y += Newton(power - 1, i) * Math.Pow(t, i) * Math.Pow((1 - t), power - 1 - i) * points[i].Y;
            }
            return p;
        }

        public void drawBezierCurve(Canvas can)
        {
            this.polyline.Points.Clear();
            can.Children.Remove(this.polyline);
            for (int i = 0; i < this.lines.Length; ++i)
            {
                can.Children.Remove(this.lines[i]);
            }
            double t = 0;
            while (t <= 1.01)
            {
                this.polyline.Points.Add(multicurve(this.points, t));
                t += 0.01;
            }
            for (int i = 0; i < this.lines.Length; ++i)
            {
                can.Children.Insert(0, this.lines[i]);
            }
            drawLines();
            can.Children.Insert(0, this.polyline);
            for (int i = 0; i < this.points.Length; ++i)
            {
                Canvas.SetTop(this.pointsEll[i], this.points[i].Y - 10);
                Canvas.SetLeft(this.pointsEll[i], this.points[i].X - 10);
            }
        }
    }
}
