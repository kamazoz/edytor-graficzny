﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool DragInProgress = false;
        List<System.Windows.Point> punkty = new List<System.Windows.Point>();
        FPolygon polygon = new FPolygon();
        FPolyline polyline = new FPolyline();
        List<FPolygon> listaPolygonow = new List<FPolygon>();
        List<FPolyline> listaPolylineow = new List<FPolyline>();
        List<BezierCurve> listaBeziera = new List<BezierCurve>();
        System.Windows.Point LastPoint;
        private double scaleX = 1.1;
        private double scaleY = 1.1;
        Ellipse kPoint = new Ellipse
        {
            Stroke = System.Windows.Media.Brushes.Black,
            Opacity = .5,
            StrokeThickness = 2,
            Fill = System.Windows.Media.Brushes.Blue,
            Height = 20,
            Width = 20,
            IsEnabled = false
        };

        enum figure { Line, Rectangle, Circle, Ellipse, Bezier, Free, Translate, Rotation, Scale, Point, CreatePolygon, CreatePolyline, Select };
        figure figureChosen = figure.Free;
        List<System.Windows.Point> pointsBezier = new List<System.Windows.Point>();
        System.Windows.Shapes.Rectangle rectF;
        Line lineF;
        BezierCurve bezier;
        int clickCount = 0;
        System.Windows.Shapes.Ellipse elliF;
        System.Windows.Shapes.Ellipse circleF;
        System.Windows.Point startPoint;

        private double prog;
        PPM ppm;
        Obraz obraz;
        public MainWindow()
        {
            InitializeComponent();
            sX.Text = scaleX.ToString();
            sY.Text = scaleY.ToString();
        }
        
        private void ShowSelectedImage(Bitmap bit, System.Windows.Controls.Image img)
        {
            MemoryStream ms = new MemoryStream();
            bit.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            img.Source = bi;
        }

        private void OtworzObraz(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog oDialog = new Microsoft.Win32.OpenFileDialog();
            oDialog.Title = "Select a JPEG file to open";
            oDialog.Filter = "All Pictures(*.jpg;*.jpeg;*.png;*.ppm)|*.jpg;*.jpeg;*.png;*.ppm|Jpeg, Png(*.png;*.jpeg)|*.png;*.jpeg|Jpg(*.jpg)|*.jpg|PPM(*.ppm)|*.ppm";

            if ((bool)oDialog.ShowDialog())
            {
                jasnosc.Visibility = Visibility.Hidden;
                string file = oDialog.FileName;
                System.IO.Stream imageStreamSource = new System.IO.FileStream(oDialog.FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                if (oDialog.FileName.EndsWith(".png") || oDialog.FileName.EndsWith(".jpeg"))
                {
                    PngBitmapDecoder decoder = new PngBitmapDecoder(imageStreamSource, BitmapCreateOptions.None, BitmapCacheOption.Default);
                    BitmapSource bitmapSource = decoder.Frames[0];
                    obraz = new Obraz(bitmapSource);
                    if(obraz.format == PixelFormats.Gray8 || obraz.format == PixelFormats.Gray4 || obraz.format == PixelFormats.Gray2)
                    {
                        MessageBox.Show("Ten format nie jest obslugiwany");
                        return;
                    }
                    this.orginal.Source = bitmapSource;
                    this.kopia.Source = bitmapSource;
                    narzedzia.IsEnabled = true;
                }
                else if (oDialog.FileName.EndsWith(".jpg")) {
                    JpegBitmapDecoder decoder = new JpegBitmapDecoder(imageStreamSource, BitmapCreateOptions.None, BitmapCacheOption.Default);
                    BitmapSource bitmapSource = decoder.Frames[0];
                    obraz = new Obraz(bitmapSource);
                    if (obraz.format == PixelFormats.Gray8 || obraz.format == PixelFormats.Gray4 || obraz.format == PixelFormats.Gray2)
                    {
                        MessageBox.Show("Ten format nie jest obslugiwany");
                        return;
                    }
                    this.orginal.Source = bitmapSource;
                    this.kopia.Source = bitmapSource;
                    narzedzia.IsEnabled = true;
                }
                else
                {
                    this.ppm = new PPM(file);
                    ShowSelectedImage(ppm.BitMap, kopia);
                    ShowSelectedImage(ppm.BitMap, orginal);
                    obraz = new Obraz(orginal.Source as BitmapSource);
                    narzedzia.IsEnabled = true;
                }
            }

        }
        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
        Bitmap GetBitmap(BitmapSource source)
        {
            Bitmap bmp = new Bitmap(source.PixelWidth, source.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            source.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }
        private void ZapiszObraz(object sender, RoutedEventArgs e)
        {
            try
            {
                if (kopia.Source == null)
                {
                    MessageBox.Show("Otworz obraz ktory chcesz zapisac jako jpg.", "Brak obrazu");
                    return;
                }
                SaveFileDialog saveFile = new SaveFileDialog();
                string file;
                saveFile.Filter = "Jpg (*.jpg)|*.jpg";
                saveFile.ShowDialog();
                if (saveFile.FileName != "")
                {
                    file = saveFile.FileName;
                }
                else
                    return;
                Compression compWindow = new Compression();
                compWindow.ShowDialog();
                if (!compWindow.save) return;



                if (!file.EndsWith(".jpg")) file += ".jpg";

                // Get a bitmap.
                ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                // Create an Encoder object based on the GUID
                // for the Quality parameter category.
                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                // Create an EncoderParameters object.
                // An EncoderParameters object has an array of EncoderParameter
                // objects. In this case, there is only one
                // EncoderParameter object in the array.
                EncoderParameters myEncoderParameters = new EncoderParameters(1);

                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, compWindow.compression);
                myEncoderParameters.Param[0] = myEncoderParameter;

                Bitmap toSave = Obraz.GetBitmap(kopia.Source as BitmapSource);
                toSave.Save(file, jpgEncoder, myEncoderParameters);
            }
            catch (Exception)
            {
                MessageBox.Show("Nie mozna nadpisac obrazka z ktorego zapisujesz.", "Blad zapisu");
            }
        }
        private void ZamknijProgram(object sender, RoutedEventArgs e)
        {
            foreach (Window w in System.Windows.Application.Current.Windows)
            {
                w.Close();
            }
        }



        //%Zieleni
        double mod(double x, double m)
        {
            return (x % m + m) % m;
        }
        private void zielen_Click(object sender, RoutedEventArgs e)
        {
            Obraz obraz = new Obraz(kopia.Source as BitmapSource);
            byte[] img = obraz.PobierzDaneRGB();
            //double total = obraz.pixelHeight * obraz.pixelWidth;
            double percent = 0;
            double total = 0;
            float maxr = 0;
            float maxb = 0;
            float mina = 256;
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                if (maxr < img[i + 2])
                    maxr = img[i + 2];
                if(maxb < img[i])
                    maxb = img[i];
                if (mina > img[i + 3])
                    mina = img[i + 3];
                if (img[i + 3] != 0)
                    ++total;
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                
                double a3 = (1000 * ((double)img[i + 2] - maxr / 2.0) + maxr / 2.0);
                double a = (1000 * ((double)img[i ] - maxb / 2.0) + maxb / 2.0);
                img[i + 2] = (byte)Math.Max(0, Math.Min(255, a3));
                img[i ] = (byte)Math.Max(0, Math.Min(255, a));
            }
            for (int i = 0; i < obraz.dataLength; i += 4)
            {
                if (img[i] == 0 && img[i + 1] != 0 && img[i + 2] == 0)
                    percent++;
            }
            
            percent = percent * 100 / total;
            kopia.Source = obraz.KlonujObraz(img);
            zielLabel.Content = percent.ToString();
        }






        //MenuItems
        private void PrzeksztalceniaPunktowe(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Punkty.Visibility = Visibility.Visible;
        }
        private void PrzestrzenieBarwCMYK(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            CMYK.Visibility = Visibility.Visible;
        }
        private void PrzestrzenieBarwRGB(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            RGB.Visibility = Visibility.Visible;
        }
        private void HistogramyClick(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Histogramy.Visibility = Visibility.Visible;

        }
        private void FiltryClick(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            FiltryDP.Visibility = Visibility.Visible;
        }
        private void Binarization_Click(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Binaryzacja.Visibility = Visibility.Visible;
        }
        private void morf_Click(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Morf.Visibility = Visibility.Visible;
        }
        private void Zielen_Click(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Zielen.Visibility = Visibility.Visible;
        }
        private void ToolBarClick(object sender, RoutedEventArgs e)
        {
            if (ToolBars.Visibility == Visibility.Visible)
                ToolBars.Visibility = Visibility.Hidden;
            else
                ToolBars.Visibility = Visibility.Visible;
            figureChosen = figure.Free;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }
        private void Przeksztalcenia_Click(object sender, RoutedEventArgs e)
        {
            HideDockPanel();
            Przeksztalcenia2D.Visibility = Visibility.Visible;
        }
        private void powiekszOrginal(object sender, RoutedEventArgs e)
        {
            kopia.Visibility = Visibility.Collapsed;
            orginal.Visibility = Visibility.Visible;
            Grid.SetColumnSpan(canvaBorder, 2);

        }
        private void powiekszKopie(object sender, RoutedEventArgs e)
        {
            orginal.Visibility = Visibility.Collapsed;
            kopia.Visibility = Visibility.Visible;
            Grid.SetColumnSpan(kopia, 2);
            Grid.SetColumn(kopia, 1);
        }
        private void powiekszOba(object sender, RoutedEventArgs e)
        {
            orginal.Visibility = Visibility.Visible;
            kopia.Visibility = Visibility.Visible;
            Grid.SetColumnSpan(kopia, 1);
            Grid.SetColumnSpan(canvaBorder, 1);
            Grid.SetColumn(kopia, 2);
        }
        private void OrginalnyObrazek(object sender, RoutedEventArgs e)
        {
            if (orginal.Source != null)
                orginal.Source = obraz.original;
            jasnosc.Visibility = Visibility.Hidden;
        }
        private void ZatwierdzZmiany(object sender, RoutedEventArgs e)
        {
            if (orginal.Source != null)
            {
                orginal.Source = kopia.Source;
                obraz = new Obraz(kopia.Source as BitmapSource);
            }
        }
        private void PrzeladujObrazek(object sender, RoutedEventArgs e)
        {
            if (orginal.Source != null)
                kopia.Source = obraz.original;
        }

        private void HideDockPanel()
        {
            foreach(DockPanel dp in parent.Children)
                dp.Visibility = Visibility.Hidden;
            if(obraz != null)
                orginal.Source = obraz.original;
            jasnosc.Visibility = Visibility.Hidden;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            okno.Close();
            hsv.Close();
            Cubergb.Close();
            rgbToCmyk.Close();
        }

        
    }
}