﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for Dowolny.xaml
    /// </summary>
    public partial class Dowolny : Window
    {
        public float[,] mask;
        public float norm;
        public int row;
        public int col;
        public bool closed;
        public bool proceed = false;

        public Dowolny()
        {
            closed = false;
            InitializeComponent();
            stack.Rows = 3;
            stack.Columns = 3;
            TextBox[,] test = new TextBox[3, 3];
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    test[i, j] = new TextBox();
                    test[i, j].Text = "1";
                    stack.Children.Add(test[i, j]);
                }
            }
        }

        private void kolBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int valueK = 0;
            int valueR = 0;
            bool parsedKol = int.TryParse(kolBox.Text, out valueK);
            bool parsedRow;
            if (rowBox != null)
                parsedRow = int.TryParse(rowBox.Text, out valueR);
            else
                parsedRow = false;

            if (parsedKol && parsedRow && valueR > 0 && valueR % 2 == 1 && valueK > 0 && valueK % 2 == 1 && stack != null)
            {
                stack.Children.Clear();
                stack.Columns = valueK;
                stack.Rows = valueR;
                TextBox[,] test = new TextBox[valueR, valueK];
                for (int i = 0; i < valueR; ++i)
                {
                    for (int j = 0; j < valueK; ++j)
                    {
                        test[i, j] = new TextBox();
                        test[i, j].Text = "1";
                        stack.Children.Add(test[i, j]);
                    }
                }
            }
        }

        private void rowBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int valueK = 0;
            int valueR = 0;
            bool parsedKol = int.TryParse(kolBox.Text, out valueK);
            bool parsedRow = int.TryParse(rowBox.Text, out valueR);

            if (parsedKol && parsedRow && valueR > 0 && valueR % 2 == 1 && valueK > 0 && valueK % 2 == 1 && stack != null)
            {

                stack.Children.Clear();
                stack.Columns = valueK;
                stack.Rows = valueR;
                TextBox[,] test = new TextBox[valueR, valueK];
                for (int i = 0; i < valueR; ++i)
                {
                    for (int j = 0; j < valueK; ++j)
                    {
                        test[i, j] = new TextBox();
                        test[i, j].Text = "1";
                        stack.Children.Add(test[i, j]);
                    }
                }
            }
        }

        private void anuluj_Click(object sender, RoutedEventArgs e)
        {
            closed = true;
            Close();
        }

        private void stworz_Click(object sender, RoutedEventArgs e)
        {
            int r = stack.Rows;
            int c = stack.Columns;
            int vr = 0;
            bool parseR = int.TryParse(rowBox.Text, out vr);
            int vk = 0;
            bool parseK = int.TryParse(kolBox.Text, out vk);
            if (!parseR || vr % 2 == 0 || !parseK || vk % 2 == 0 || vk < 0 || vr < 0)
            {
                MessageBox.Show("Kolumny i wiersze powinny byc nieparzyste oraz wieksze od zera.", "Popraw");
                proceed = false;
                return;
            }
            row = r;
            col = c;
            mask = new float[r, c];
            int x = 0;
            int y = 0;

            foreach (TextBox txt in stack.Children)
            {
                if (x == c)
                {
                    x = 0;
                    ++y;
                }
                float value;
                bool parse = float.TryParse(txt.Text, out value);
                if (!parse)
                {
                    MessageBox.Show("Zawartosc w polu: \nKolumna: " + (x + 1) + "\nWiersz: " + (y + 1), "Popraw");
                    proceed = false;
                    return;
                }
                mask[y, x] = value;
                ++x;
            }
            norm = 0;
            for (int i = 0; i < r; ++i)
            {
                for (int j = 0; j < c; ++j)
                {
                    norm += mask[i, j];
                }
            }
            if (norm == 0) norm = 1;
            var Parent = this.Owner as MainWindow;
            proceed = true;
            Parent.dowolny_Click(sender, e);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            closed = true;
        }
    }
}
