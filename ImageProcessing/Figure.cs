﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {

        //Przeksztalcenia 2D
        private void imgCanva_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            switch (figureChosen)
            {
                case figure.Rotation:
                    double v;
                    bool parsed = false;
                    parsed = double.TryParse(degree.Text, out v);
                    if (parsed)
                    {
                        if (e.Delta > 0)
                            Rotate(v);
                        else
                            Rotate(-v);
                    }
                    break;
                case figure.Scale:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl) && Keyboard.IsKeyDown(Key.LeftShift))
                    {
                        if (e.Delta > 0)
                            scaleX += 0.1;
                        else
                            scaleX -= 0.1;
                    }
                    else if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        if (e.Delta > 0)
                            scaleY += 0.1;
                        else
                            scaleY -= 0.1;
                    }
                    else if (e.Delta > 0)
                    {
                        scaleX += 0.1;
                        scaleY += 0.1;
                    }
                    else
                    {
                        scaleX -= 0.1;
                        scaleY -= 0.1;
                    }
                    sX.Text = scaleX.ToString();
                    sY.Text = scaleY.ToString();
                    break;
            }
        }
        private void center(object sender, RoutedEventArgs e)
        {
            double numOfSelected = 0;
            double centerX = 0;
            double centerY = 0;
            foreach (var item in listaPolygonow)
            {
                if (item.getSelectedBool())
                {
                    numOfSelected++;
                    System.Windows.Point a = item.center();
                    centerX += a.X;
                    centerY += a.Y;
                }
            }
            foreach (var item in listaPolylineow)
            {
                if (item.getSelectedBool())
                {
                    numOfSelected++;
                    System.Windows.Point a = item.center();
                    centerX += a.X;
                    centerY += a.Y;
                }
            }
            if (numOfSelected > 0)
            {
                centerX /= numOfSelected;
                centerY /= numOfSelected;
            }
            LastPoint = new System.Windows.Point(centerX, centerY);
            Canvas.SetTop(kPoint, LastPoint.Y - 10);
            Canvas.SetLeft(kPoint, LastPoint.X - 10);
            imgCanva.Children.Remove(kPoint);
            imgCanva.Children.Add(kPoint);
        }
        private void Translate(MouseEventArgs e)
        {
            System.Windows.Point trans = e.GetPosition(imgCanva);
            double offset_X = -(LastPoint.X - trans.X);
            double offset_Y = -(LastPoint.Y - trans.Y);
            foreach (var item in listaPolygonow)
            {
                if (item.getSelectedBool())
                {
                    item.Translate(new System.Windows.Point(offset_X, offset_Y));
                    imgCanva.Children.Remove(item.getPolygon());
                    imgCanva.Children.Add(item.getPolygon());
                }
            }
            foreach (var item in listaPolylineow)
            {
                if (item.getSelectedBool())
                {
                    item.Translate(new System.Windows.Point(offset_X, offset_Y));
                    imgCanva.Children.Remove(item.getPolyline());
                    imgCanva.Children.Add(item.getPolyline());
                }
            }
            LastPoint = trans;
        }
        private void Scale()
        {
            double sx, sy;
            bool parsx, parsy;
            parsx = double.TryParse(sX.Text, out sx);
            parsy = double.TryParse(sY.Text, out sy);
            if (parsx && parsy)
            {
                foreach (var item in listaPolygonow)
                {
                    if (item.getSelectedBool())
                    {
                        item.Scale(new System.Windows.Point(sx, sy), LastPoint);
                        imgCanva.Children.Remove(item.getPolygon());
                        imgCanva.Children.Add(item.getPolygon());
                    }
                }
                foreach (var item in listaPolylineow)
                {
                    if (item.getSelectedBool())
                    {
                        item.Scale(new System.Windows.Point(sx, sy), LastPoint);
                        imgCanva.Children.Remove(item.getPolyline());
                        imgCanva.Children.Add(item.getPolyline());
                    }
                }
            }
        }
        private void Rotate(double angle)
        {

            foreach (var item in listaPolygonow)
            {
                if (item.getSelectedBool())
                {
                    item.Rotate(angle, LastPoint);
                    imgCanva.Children.Remove(item.getPolygon());
                    imgCanva.Children.Add(item.getPolygon());
                }
            }
            foreach (var item in listaPolylineow)
            {
                if (item.getSelectedBool())
                {
                    item.Rotate(angle, LastPoint);
                    imgCanva.Children.Remove(item.getPolyline());
                    imgCanva.Children.Add(item.getPolyline());
                }
            }
        }
    }
    abstract class Figure
    {
        protected bool selectedBool = false;
        public bool getSelectedBool()
        {
            return selectedBool;
        }
        protected double[][] xyn;
        protected double[] matrix = new double[9];
        public Point center()
        {
            double maxX = Double.MinValue;
            double minX = Double.MaxValue;
            double maxY = Double.MinValue;
            double minY = Double.MaxValue;
            for (int i = 0; i < xyn.Length; ++i)
            {
                if (maxX < xyn[i][0])
                {
                    maxX = xyn[i][0];
                }
                if (minX > xyn[i][0])
                {
                    minX = xyn[i][0];
                }
                if (maxY < xyn[i][1])
                {
                    maxY = xyn[i][1];
                }
                if (minY > xyn[i][1])
                {
                    minY = xyn[i][1];
                }
            }
            return new Point((maxX + minX) / 2.0, (maxY + minY) / 2.0);
        }
        public double[] pomnoz(double[] matrix, double[] vector)
        {
            int a = -1;
            double[] newVector = new double[3];
            for (int i = 0; i < matrix.Length; ++i)
            {
                if (i % 3 == 0)
                {
                    a++;
                }
                newVector[a] += matrix[i] * vector[i % 3];
            }
            return newVector;
        }
        protected void identity()
        {
            for (int i = 0; i < matrix.Length; ++i)
            {
                matrix[i] = 0;
            }
            matrix[0] = matrix[4] = matrix[8] = 1;
        }
        public abstract void Translate(Point p);
        public abstract void Rotate(double angle, Point p);
        public abstract void Scale(Point scaleTo, Point scalePoint);


    }
    class FPolyline : Figure
    {
        protected Polyline figure = new Polyline { Stroke = Brushes.Black, StrokeThickness = 2, };
        public FPolyline(List<Point> points)
        {
            figure.MouseDown += selectedFigure;
            xyn = new double[points.Count][];
            for (int i = 0; i < points.Count; ++i)
            {
                xyn[i] = new double[3];
                xyn[i][0] = points[i].X;
                xyn[i][1] = points[i].Y;
                xyn[i][2] = 1;
            }
            setPolyline();
        }
        public FPolyline()
        {
        }
        private void setPolyline()
        {
            figure.Points.Clear();
            for (int i = 0; i < xyn.Length; ++i)
            {
                figure.Points.Add(new Point(xyn[i][0], xyn[i][1]));
            }
        }
        public override void Translate(Point p)
        {
            identity();
            matrix[2] = p.X;
            matrix[5] = p.Y;

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolyline();
        }

        public override void Rotate(double angle, Point rotatePoint)
        {
            identity();
            double radians = angle * (Math.PI / 180);
            matrix[0] = Math.Cos(radians);
            matrix[1] = -Math.Sin(radians);
            matrix[3] = Math.Sin(radians);
            matrix[4] = Math.Cos(radians);
            matrix[2] = -rotatePoint.X * Math.Cos(radians) + rotatePoint.Y * Math.Sin(radians) + rotatePoint.X;
            matrix[5] = -rotatePoint.X * Math.Sin(radians) - rotatePoint.Y * Math.Cos(radians) + rotatePoint.Y;

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolyline();
        }

        public override void Scale(Point scaleTo, Point scalePoint)
        {
            identity();
            matrix[0] = scaleTo.X;
            matrix[4] = scaleTo.Y;
            matrix[2] = scalePoint.X * (1 - scaleTo.X);
            matrix[5] = scalePoint.Y * (1 - scaleTo.Y);

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolyline();
        }
        public Polyline getPolyline()
        {
            return figure;
        }
        public void selectedFigure(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftShift) == true && e.LeftButton == MouseButtonState.Pressed && selectedBool == false)
            {
                selectedBool = true;
                figure.Fill = Brushes.LightBlue;
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) == true && e.LeftButton == MouseButtonState.Pressed && selectedBool == true)
            {
                selectedBool = false;
                figure.Fill = null;
            }
        }
    }
    class FPolygon : Figure
    {
        protected Polygon figure = new Polygon { Stroke = Brushes.Black, StrokeThickness = 2, Fill = Brushes.White };
        public FPolygon(List<Point> points)
        {
            figure.MouseDown += selectedFigure;
            xyn = new double[points.Count][];
            for (int i = 0; i < points.Count; ++i)
            {
                xyn[i] = new double[3];
                xyn[i][0] = points[i].X;
                xyn[i][1] = points[i].Y;
                xyn[i][2] = 1;
            }
            setPolygon();
        }
        public FPolygon()
        {
        }
        private void setPolygon()
        {
            figure.Points.Clear();
            for (int i = 0; i < xyn.Length; ++i)
            {
                figure.Points.Add(new Point(xyn[i][0], xyn[i][1]));
            }
        }
        public override void Translate(Point p)
        {
            identity();
            matrix[2] = p.X;
            matrix[5] = p.Y;

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolygon();
        }

        public override void Rotate(double angle, Point rotatePoint)
        {
            identity();
            double radians = angle * (Math.PI / 180);
            matrix[0] = Math.Cos(radians);
            matrix[1] = -Math.Sin(radians);
            matrix[3] = Math.Sin(radians);
            matrix[4] = Math.Cos(radians);
            matrix[2] = -rotatePoint.X * Math.Cos(radians) + rotatePoint.Y * Math.Sin(radians) + rotatePoint.X;
            matrix[5] = -rotatePoint.X * Math.Sin(radians) - rotatePoint.Y * Math.Cos(radians) + rotatePoint.Y;

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolygon();
        }

        public override void Scale(Point scaleTo, Point scalePoint)
        {
            identity();
            matrix[0] = scaleTo.X;
            matrix[4] = scaleTo.Y;
            matrix[2] = scalePoint.X * (1 - scaleTo.X);
            matrix[5] = scalePoint.Y * (1 - scaleTo.Y);

            for (int i = 0; i < xyn.Length; ++i)
                xyn[i] = pomnoz(matrix, xyn[i]);
            setPolygon();
        }
        public Polygon getPolygon()
        {
            return figure;
        }
        public void selectedFigure(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftShift) == true && e.LeftButton == MouseButtonState.Pressed && selectedBool == false)
            {
                selectedBool = true;
                figure.Fill = Brushes.LightBlue;
            }
            if (Keyboard.IsKeyDown(Key.LeftCtrl) == true && e.LeftButton == MouseButtonState.Pressed && selectedBool == true)
            {
                selectedBool = false;
                figure.Fill = Brushes.White;
            }
        }
    }
}
