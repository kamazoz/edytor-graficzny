﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for Cube.xaml
    /// </summary>
    public partial class Cube : Window
    {
        public bool closed;
        public Cube()
        {
            closed = false;
            InitializeComponent();
            rgbCube();
        }
        private void ShowImage(System.Windows.Controls.Image img, Bitmap bit)
        {
            MemoryStream ms = new MemoryStream();
            bit.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            img.Source = bi;
        }
        private void rgbCube()
        {
            Bitmap im = new Bitmap(255, 255);
            int firstColor = 256;
            int secondColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor--;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(255, firstColor, 0, x);
                    im.SetPixel(x, y, color);
                }
            }

            ShowImage(image1, im);

            firstColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor = 255;
                secondColor--;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(firstColor--, firstColor, y, 0);
                    im.SetPixel(x, y, color);
                }
            }
            ShowImage(image2, im);

            firstColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor--;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(255, 0, y, x);
                    im.SetPixel(x, y, color);
                }
            }

            ShowImage(image3, im);

            firstColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor--;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(255, x, y, 255);
                    im.SetPixel(x, y, color);
                }
            }
            ShowImage(image4, im);

            firstColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor = 255;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(255, 255, y, firstColor--);
                    im.SetPixel(x, y, color);
                }
            }
            ShowImage(image5, im);

            firstColor = 256;
            secondColor = 256;
            for (int y = 0; y < 255; ++y)
            {
                firstColor = 255;
                secondColor--;
                for (int x = 0; x < 255; ++x)
                {

                    System.Drawing.Color color = System.Drawing.Color.FromArgb(255, y, 255, x);
                    im.SetPixel(x, y, color);
                }
            }
            ShowImage(image6, im);

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            closed = true;
        }
    }
}
