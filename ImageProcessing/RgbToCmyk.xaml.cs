﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for RgbToCmyk.xaml
    /// </summary>
    /// 
    public partial class RgbToCmyk : Window
    {
        Bitmap bitmap = new Bitmap(100, 50);
        public bool closed;
        public RgbToCmyk()
        {
            InitializeComponent();
            r.Text = "255";
            g.Text = "255";
            b.Text = "255";
            c.Text = "0";
            m.Text = "0";
            y.Text = "0";
            k.Text = "0";
        }
        private void ShowImage(System.Windows.Controls.Image img, Bitmap bit)
        {
            MemoryStream ms = new MemoryStream();
            bit.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            img.Source = bi;
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            closed = true;
            Close();
        }
        private void cmykChanged()
        {
            byte R, G, B;
            float C, M, Y, K;
            if (float.TryParse(c.Text, out C) && float.TryParse(m.Text, out M) && float.TryParse(y.Text, out Y) && float.TryParse(k.Text, out K))
            {
                if (C > 1) C = 1;
                if (M > 1) M = 1;
                if (Y > 1) Y = 1;
                if (K > 1) K = 1;
                if (C < 0) C = 0;
                if (M < 0) M = 0;
                if (Y < 0) Y = 0;
                if (K < 0) K = 0;
                R = (byte) (255 * (1 - C) * (1 - K));
                G = (byte) (255 * (1 - M) * (1 - K));
                B = (byte) (255 * (1 - Y) * (1 - K));
                r.Text = R.ToString();
                g.Text = G.ToString();
                b.Text = B.ToString();
                for(int y = 0; y < 50; ++y)
                {
                    for(int x = 0; x < 100; ++x)
                    {
                        System.Drawing.Color col = System.Drawing.Color.FromArgb(R, G, B);
                        bitmap.SetPixel(x, y, col);
                    }
                }
                ShowImage(img, bitmap);
            }
        }

        private void rgbChanged()
        {
            byte R, G, B;
            float Rf, Gf, Bf;
            float C, M, Y, K;
            if(byte.TryParse(r.Text, out R) && byte.TryParse(g.Text, out G) && byte.TryParse(b.Text, out B))
            {
                if (R > 255) R = 255;
                if (G > 255) G = 255;
                if (B > 255) B = 255;
                if (R < 0) R = 0;
                if (G < 0) G = 0;
                if (B < 0) B = 0;

                if (R == 0 && G == 0 && B == 0)
                {
                    c.Text = "0";
                    m.Text = "0";
                    y.Text = "0";
                    k.Text = "1";
                    for (int y = 0; y < 50; ++y)
                    {
                        for (int x = 0; x < 100; ++x)
                        {
                            System.Drawing.Color col = System.Drawing.Color.FromArgb(0, 0, 0);
                            bitmap.SetPixel(x, y, col);
                        }
                    }
                }
                else
                {
                    Rf = R / 255f;
                    Gf = G / 255f;
                    Bf = B / 255f;
                    K = 1 - Math.Max(Rf, Math.Max(Gf, Bf));
                    C = (1 - Rf - K) / (1 - K);
                    M = (1 - Gf - K) / (1 - K);
                    Y = (1 - Bf - K) / (1 - K);
                    C = (float)decimal.Round((decimal)C, 2, MidpointRounding.AwayFromZero);
                    M = (float)decimal.Round((decimal)M, 2, MidpointRounding.AwayFromZero);
                    Y = (float)decimal.Round((decimal)Y, 2, MidpointRounding.AwayFromZero);
                    K = (float)decimal.Round((decimal)K, 2, MidpointRounding.AwayFromZero);
                    c.Text = C.ToString();
                    m.Text = M.ToString();
                    y.Text = Y.ToString();
                    k.Text = K.ToString();
                    for (int y = 0; y < 50; ++y)
                    {
                        for (int x = 0; x < 100; ++x)
                        {
                            System.Drawing.Color col = System.Drawing.Color.FromArgb(R, G, B);
                            bitmap.SetPixel(x, y, col);
                        }
                    }
                }
                ShowImage(img, bitmap);

            }
        }

        private void c_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            cmykChanged();

        }

        private void m_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            cmykChanged();
        }

        private void y_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            cmykChanged();
        }

        private void k_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            cmykChanged();
        }

        private void r_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            rgbChanged();
        }

        private void g_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            rgbChanged();
        }

        private void b_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            rgbChanged();
        }

        private void b_LostFocus(object sender, RoutedEventArgs e)
        {
            rgbChanged();
        }

        private void g_LostFocus(object sender, RoutedEventArgs e)
        {
            rgbChanged();
        }

        private void r_LostFocus(object sender, RoutedEventArgs e)
        {
            rgbChanged();
        }

        private void k_LostFocus(object sender, RoutedEventArgs e)
        {
            cmykChanged();
        }

        private void y_LostFocus(object sender, RoutedEventArgs e)
        {
            cmykChanged();
        }

        private void m_LostFocus(object sender, RoutedEventArgs e)
        {
            cmykChanged();
        }

        private void c_LostFocus(object sender, RoutedEventArgs e)
        {
            cmykChanged();
        }
    }
}
