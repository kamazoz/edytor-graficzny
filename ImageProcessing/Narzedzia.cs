﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace ImageProcessing
{
    public partial class MainWindow : Window
    {

        //Tools
        private void line_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Line;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }

        private void rect_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Rectangle;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }

        private void circle_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Circle;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }

        private void elipse_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Ellipse;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }
        private void Bezier_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Bezier;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.showPoints(imgCanva);
                }
        }
        private void Clean_Click(object sender, RoutedEventArgs e)
        {
            imgCanva.Children.RemoveRange(0, imgCanva.Children.Count);
            listaPolygonow.Clear();
            listaPolylineow.Clear();
            listaBeziera.Clear();
            imgCanva.Children.Add(orginal);
            figureChosen = figure.Free;
        }
        private void rotating(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Rotation;
        }
        private void CreatePolyline_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.CreatePolyline;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }
        private void CreatePolygon_Click(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.CreatePolygon;
            if (bezier != null)
                foreach (BezierCurve b in listaBeziera)
                {
                    b.hidePoints(imgCanva);
                }
        }

        private void scaling(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Scale;

        }

        private void translating(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Translate;

        }

        private void setPoint(object sender, RoutedEventArgs e)
        {
            figureChosen = figure.Point;
        }


        private void imgCanva_MouseDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(imgCanva);
            DragInProgress = true;
            switch (figureChosen)
            {
                case figure.Select:
                    foreach (var item in listaPolygonow)
                    {
                        imgCanva.Children.Remove(item.getPolygon());
                        imgCanva.Children.Add(item.getPolygon());
                    }
                    break;
                case figure.Translate:
                    imgCanva.Children.Remove(kPoint);
                    LastPoint = e.GetPosition(imgCanva);
                    break;
                case figure.Rotation:
                    break;
                case figure.Scale:
                    Scale();
                    break;
                case figure.Point:
                    Canvas.SetTop(kPoint, e.GetPosition(imgCanva).Y - 10);
                    Canvas.SetLeft(kPoint, e.GetPosition(imgCanva).X - 10);
                    imgCanva.Children.Remove(kPoint);
                    imgCanva.Children.Add(kPoint);
                    LastPoint = e.GetPosition(imgCanva);
                    break;
                case figure.CreatePolygon:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl) == true)
                    {
                        punkty.Add(e.GetPosition(imgCanva));
                    }
                    else
                    {
                        if (punkty.Count <= 0) return;
                        polygon = new FPolygon(punkty);
                        listaPolygonow.Add(polygon);
                        imgCanva.Children.Add(polygon.getPolygon());
                        punkty.Clear();
                    }
                    break;
                case figure.CreatePolyline:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl) == true)
                    {
                        punkty.Add(e.GetPosition(imgCanva));
                    }
                    else
                    {
                        if (punkty.Count <= 0) return;
                        polyline = new FPolyline(punkty);
                        listaPolylineow.Add(polyline);
                        imgCanva.Children.Add(polyline.getPolyline());
                        punkty.Clear();
                    }
                    break;
                case figure.Line:
                    lineF = new Line
                    {
                        Stroke = System.Windows.Media.Brushes.Black,
                        StrokeThickness = 2,
                        IsHitTestVisible = false
                    };
                    lineF.X1 = startPoint.X;
                    lineF.Y1 = startPoint.Y;
                    lineF.X2 = startPoint.X;
                    lineF.Y2 = startPoint.Y;
                    imgCanva.Children.Add(lineF);
                    break;

                case figure.Rectangle:
                    rectF = new System.Windows.Shapes.Rectangle
                    {
                        Stroke = System.Windows.Media.Brushes.Black,
                        StrokeThickness = 2,
                        IsHitTestVisible = false
                    };
                    Canvas.SetLeft(rectF, startPoint.X);
                    Canvas.SetTop(rectF, startPoint.X);
                    imgCanva.Children.Add(rectF);
                    break;

                case figure.Circle:
                    circleF = new System.Windows.Shapes.Ellipse
                    {
                        Stroke = System.Windows.Media.Brushes.Black,
                        StrokeThickness = 2,
                        IsHitTestVisible = false
                    };
                    Canvas.SetLeft(circleF, startPoint.X);
                    Canvas.SetTop(circleF, startPoint.Y);
                    imgCanva.Children.Add(circleF);
                    break;

                case figure.Ellipse:
                    elliF = new System.Windows.Shapes.Ellipse
                    {
                        Stroke = System.Windows.Media.Brushes.Black,
                        StrokeThickness = 2,
                        IsHitTestVisible = false
                    };
                    Canvas.SetLeft(elliF, startPoint.X);
                    Canvas.SetTop(elliF, startPoint.Y);
                    imgCanva.Children.Add(elliF);
                    break;
                case figure.Bezier:
                    if (e.RightButton == MouseButtonState.Released)
                    {
                        if (clickCount > 0)
                        {
                            System.Windows.Point[] point = new System.Windows.Point[clickCount];
                            for (int i = 0; i < clickCount; ++i)
                            {
                                point[i] = new System.Windows.Point(pointsBezier[i].X, pointsBezier[i].Y);
                            }
                            bezier = new BezierCurve(point);
                            listaBeziera.Add(bezier);
                            addEvents();
                            pointsBezier.Clear();
                        }
                        clickCount = 0;
                        return;
                    }

                    if (e.LeftButton == MouseButtonState.Pressed)
                    {
                        pointsBezier.Add(e.GetPosition(imgCanva));
                        ++clickCount;
                    }
                    break;
                case figure.Free:
                    if (bezier != null)
                        foreach (BezierCurve b in listaBeziera)
                        {
                            b.hidePoints(imgCanva);
                        }
                    break;
            }
        }

        private void imgCanva_MouseMove(object sender, MouseEventArgs e)
        {
            if (DragInProgress)
            {
                switch (figureChosen)
                {
                    case figure.Translate:
                        Translate(e);
                        break;
                    case figure.Rotation:
                        double v;
                        bool parsed = false;
                        parsed = double.TryParse(degree.Text, out v);
                        if (parsed)
                            Rotate(v);
                        break;
                    case figure.Scale:
                        break;
                    case figure.Point:
                        Canvas.SetTop(kPoint, e.GetPosition(imgCanva).Y - 10);
                        Canvas.SetLeft(kPoint, e.GetPosition(imgCanva).X - 10);
                        imgCanva.Children.Remove(kPoint);
                        imgCanva.Children.Add(kPoint);
                        LastPoint = e.GetPosition(imgCanva);
                        break;

                }
            }
            if (e.LeftButton == MouseButtonState.Released)
                return;

            var pos = e.GetPosition(imgCanva);
            var x = 0.0;
            var y = 0.0;
            switch (figureChosen)
            {
                case figure.Line:
                    if (lineF == null) return;
                    x = pos.X;
                    y = pos.Y;
                    lineF.X2 = x;
                    lineF.Y2 = y;
                    //figures.Add(lineF);
                    break;

                case figure.Rectangle:
                    if (rectF == null) return;

                    x = Math.Min(pos.X, startPoint.X);
                    y = Math.Min(pos.Y, startPoint.Y);

                    var w = Math.Max(pos.X, startPoint.X) - x;
                    var h = Math.Max(pos.Y, startPoint.Y) - y;

                    rectF.Width = w;
                    rectF.Height = h;

                    Canvas.SetLeft(rectF, x);
                    Canvas.SetTop(rectF, y);
                    break;

                case figure.Circle:

                    if (pos.X < startPoint.X)
                    {
                        circleF.Width = startPoint.X - pos.X;
                        circleF.Height = startPoint.X - pos.X;
                        Canvas.SetLeft(circleF, pos.X);
                        Canvas.SetTop(circleF, startPoint.Y);
                    }
                    else
                    {
                        circleF.Width = pos.X - startPoint.X;
                        circleF.Height = pos.X - startPoint.X;
                        Canvas.SetLeft(circleF, startPoint.X);
                        Canvas.SetTop(circleF, startPoint.Y);
                    }
                    
                    break;

                case figure.Ellipse:
                    x = Math.Min(startPoint.X, pos.X);
                    y = Math.Min(startPoint.Y, pos.Y);
                    elliF.Width = Math.Abs(startPoint.X - pos.X);
                    elliF.Height = Math.Abs(startPoint.Y - pos.Y);
                    Canvas.SetLeft(elliF, x);
                    Canvas.SetTop(elliF, y);
                    break;
                case figure.Bezier:
                    if (bezier == null) return;
                    foreach (BezierCurve b in listaBeziera)
                    {
                        for (int i = 0; i < b.pointsEll.Length; ++i)
                        {
                            if (b.pressed[i] == true)
                            {
                                b.points[i].X = e.GetPosition(imgCanva).X;
                                b.points[i].Y = e.GetPosition(imgCanva).Y;
                                break;
                            }
                        }
                        b.drawBezierCurve(imgCanva);
                    }
                    break;
                case figure.Free:
                    break;
            }
        }
        private void imgCanva_MouseUp(object sender, MouseButtonEventArgs e)
        {
            DragInProgress = false;
            switch (figureChosen)
            {
                case figure.Line:
                    break;

                case figure.Rectangle:
                    break;

                case figure.Circle:
                    break;

                case figure.Ellipse:
                    break;
                case figure.Bezier:
                    if (bezier == null) return;
                    foreach (BezierCurve b in listaBeziera)
                    {
                        for (int i = 0; i < b.pressed.Length; ++i)
                        {
                            b.pressed[i] = false;
                        }
                    }
                    break;
                case figure.Free:
                    break;
            }
        }
    }
    class Narzedzia
    {
    }
}
