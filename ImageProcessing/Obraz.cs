﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows;

namespace ImageProcessing
{
    public class Obraz
    {
        public BitmapSource original { get; set; }
        public int dataLength { get; set; }
        public int stride { get; set; }
        public int pixelWidth { get; set; }
        public int pixelHeight { get; set; }
        public double dpiX { get; set; }
        public double dpiY { get; set; }
        public System.Windows.Media.PixelFormat format { get; set; }
        public float[] hist{ get; set; }

        public Obraz(BitmapSource imageBms)
        {
            original = imageBms;
            stride = imageBms.PixelWidth * ((imageBms.Format.BitsPerPixel + 7) / 8);
            dataLength = stride * imageBms.PixelHeight;
            pixelWidth = imageBms.PixelWidth;
            pixelHeight = imageBms.PixelHeight;
            dpiX = imageBms.DpiX;
            dpiY = imageBms.DpiY;
            format = imageBms.Format;
            hist = new float[256];

            byte[] rgb = this.PobierzDaneRGB();
            for (int i = 0; i < dataLength; i += 4)
            {
                int gray = (int)((rgb[i] + rgb[i + 1] + rgb[i + 2]) / 3.0);
                ++hist[gray];
            }
        }


        public BitmapSource KlonujObraz(byte[] obraz)
        {

            BitmapSource klonObrazu = BitmapSource.Create(pixelWidth, pixelHeight, dpiX, dpiY, format, null, obraz, stride);
            return klonObrazu;
        }


        public byte[] PobierzDaneRGB()
        {
            byte[] rgb = new byte[dataLength];
            original.CopyPixels(rgb, stride, 0);
            return rgb;
        }

        static public Bitmap GetBitmap(BitmapSource source)
        {
            Bitmap bmp = new Bitmap(source.PixelWidth, source.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size), ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            source.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }
    }
}
