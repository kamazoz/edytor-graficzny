﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageProcessing
{
    /// <summary>
    /// Interaction logic for HSV.xaml
    /// </summary>
    public partial class HSV : Window
    {

        public bool closed; 
        public HSV()
        {
            closed = false;
            InitializeComponent();
            rysujPanelHSV();
        }
        private void ShowImage(System.Windows.Controls.Image img, Bitmap bit)
        {
            MemoryStream ms = new MemoryStream();
            bit.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            ms.Position = 0;
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            img.Source = bi;
        }
        private void rysujPanelHSV()
        {
            double V;
            double X;
            double m;
            double C;
            double Rp = 0;
            double Gp = 0;
            double Bp = 0;
            byte r;
            byte g;
            byte b;
            Bitmap bitmap = new Bitmap(360, 101);
            for(int H = 0; H < 360; ++H)
            {
                int y = 0;
                for (int S = 100; S >= 0; --S)
                {
                    V = S/100.0;
                    C = V * S/100.0;
                    X = C * (1-Math.Abs((((double)H/60.0) % 2) - 1));
                    m = V - C;
                    if(H >= 0 && H < 60)
                    {
                        Rp = C;
                        Gp = X;
                        Bp = 0;
                    }
                    else if (H >= 60 && H < 120)
                    {
                        Rp = X;
                        Gp = C;
                        Bp = 0;
                    }
                    else if (H >= 120 && H < 180)
                    {
                        Rp = 0;
                        Gp = C;
                        Bp = X;
                    }
                    else if (H >= 180 && H < 240)
                    {
                        Rp = 0;
                        Gp = X;
                        Bp = C;
                    }
                    else if (H >= 240 && H < 300)
                    {
                        Rp = X;
                        Gp = 0;
                        Bp = C;
                    }
                    else if (H >= 300 && H < 360)
                    {
                        Rp = C;
                        Gp = 0;
                        Bp = X;
                    }
                    r = (byte)((Rp + m) * 255);
                    g = (byte)((Gp + m) * 255);
                    b = (byte)((Bp + m) * 255);
                    System.Drawing.Color col = System.Drawing.Color.FromArgb(255, r, g, b);
                    bitmap.SetPixel(H, y++, col);
                }
            }
            ShowImage(image, bitmap);
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            closed = true;
        }
    }
}
